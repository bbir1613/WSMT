import java.util.*;
import java.lang.*;
import java.io.*;
class Matrix
{
 
    public static void main (String[] args) throws java.lang.Exception
    {
        if(args.length ==0) {
            System.out.println("Please provide text file path");
            return;
        }
        File file = new File(args[0]);
        if(!file.exists()){
            System.out.println("Please create the file  " +args[0]);
            return;
        } 
        System.out.println("START");
        run(args[0]);
        System.out.println("END");
    }

    static void run(String fileName){
        try (BufferedReader br = new BufferedReader(new FileReader(fileName));
             PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)))){
            String line;
            while ((line = br.readLine()) != null) {
                if(line.trim().equals(";")) break;
                String[] values = line.split(" ");
                int[] res= new int[values.length];
                for(int i =0 ;i < values.length; ++i){
                    res[i]= Integer.parseInt(values[i]);
                }
                String r = toString(selectionSort(res));
                out.println(r);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

     static int[] selectionSort(int[] arr){
        for (int i = 0; i < arr.length - 1; i++)
        {
            int index = i;
            for (int j = i + 1; j < arr.length; j++)
                if (arr[j] < arr[index])
                    index = j;
     
            int smallerNumber = arr[index];  
            arr[index] = arr[i];
            arr[i] = smallerNumber;
        }
        return arr;
    }

    static String toString(int[] v){
        String s = "";
        for(int i=0;i < v.length; ++i){
            s+=String.valueOf(v[i]);
            if(i < v.length-1) s+=" ";
        }
        return s;
    }
}
