import sys;
from pathlib import Path

def to_string(v):
	s = ""
	for i in range(len(v)):
		s+= str(v[i]) 
		if(i < len(v)-1): s+=" "
	return s

def selection_sort(arr):
    for i in range(len(arr)):
        index = i
        for j in range(i, len(arr)):
            if arr[j] < arr[index]:
                index = j
                smallerNumber = arr[index]
                arr[index] = arr[i]
                arr[i] = smallerNumber
    return arr

def run(file_name):
	br = open(file_name,'r');
	bw = open(file_name,'a');
	with br,bw:
		for line in br:
			if line == ";": break; 
			values = line.strip().split(" ")
			res = []
			for i in values:
				res.append(int(i))
			r = to_string(selection_sort(res))
			bw.write(r+"\n")


if __name__ =="__main__":
	if len(sys.argv) == 1 : raise Exception("Please provide text file path")
	file = Path(sys.argv[1])
	if not(file.exists()): raise Exception("Please create the file "+sys.argv[1])
	print("START")
	run(sys.argv[1])
	print("END")