<?php 

function main(){
	global $argc, $argv;
	if($argc == 1){
		print("Please provide text file path");
		return;
	}
	if(!file_exists($argv[1])){
		print("Please create the file $argv[1]\n");
		return;
	}
	print("START\n");
	run($argv[1]);
	print("END\n");
}

function run($file_name){
	$br = fopen($file_name,"r");
	$bw = fopen($file_name,"a");
	if($br && $bw){
		while(($line = fgets($br))!==false){
			if(strcmp($line,";") == 1) break;
			$values = preg_split('/\s+/', $line);
			$res = array();
			for($i=0; $i< count($values)-1;$i++){
				$res[]=(int)$values[$i];
			}
			fwrite($bw,to_string(selection_sort($res))."\n");
		}
	}
	fclose($br);
	fclose($bw);
}

function selection_sort($arr){
    for($i =0 ; $i < count($arr);++$i){
        $index = $i;
        for($j = $i ; $j < count($arr); ++$j){
        if($arr[$j] < $arr[$index]){
            $index = $j;
            $smallerNumber = $arr[$index];
            $arr[$index] = $arr[$i];
            $arr[$i] = $smallerNumber;
            }
        }
    }      
    return $arr;
}

function to_string($v){
        $s = "";
        for($i=0;$i < count($v); ++$i){
            $s= $s.$v[$i];
            if($i < count($v)-1) $s=$s." ";
        }
        return $s;
}

main();
?>