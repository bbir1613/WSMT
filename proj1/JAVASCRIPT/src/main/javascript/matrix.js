var fs = require("fs")

function main(){
	if(process.argv.length == 2){
		console.error("Please provide text file path");
		return;
	} 
	if(!fs.existsSync(process.argv[2])){
		console.log("Please create the file ",process.argv[2]);
		return;
	}
	run(process.argv[2]);
}

function run(file_name){
	let br = require('readline').createInterface({
		input: require('fs').createReadStream(file_name)
	});
	br.on('line',function(line){
		if(line.trim() === ";") process.exit();
		values = line.trim().split(" ");
		let res =[];
		for(let i =0 ; i< values.length;++i){
			res.push(Number(values[i]));
		}
		fs.appendFileSync(file_name, to_string(selection_sort(res))+"\n");
	});
}


function selection_sort(arr){
    for(let i =0 ; i < arr.length;++i){
        let index = i;
        for(let j = i ; j < arr.length; ++j){
        if(arr[j] < arr[index]){
            index = j
            let smallerNumber = arr[index]
            arr[index] = arr[i]
            arr[i] = smallerNumber
            }
        }
    }      
    return arr
}

function to_string(v){
        let s = "";
        for(let i=0;i < v.length; ++i){
            s+=v[i];
            if(i < v.length-1) s+=" ";
        }
        return s;
}

main();