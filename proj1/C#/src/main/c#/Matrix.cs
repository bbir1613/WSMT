using System;
using System.IO;

public class Matrix
{
    public static void Main(String[] args)
    {
        if(args.Length == 0){
            Console.WriteLine("Please provide text file path");
            return;
        }
        if(!File.Exists(args[0])){
            Console.WriteLine("Please create the file  " +args[0]);
            return;
        }
        Console.WriteLine("START");
        Run(args[0]);
        Console.WriteLine("END");
    }

    static void Run(string fileName)
    {
        using (StreamReader br = new StreamReader(new FileStream(fileName,FileMode.Open, FileAccess.Read, FileShare.ReadWrite))){
            using (StreamWriter bw = new StreamWriter(new FileStream(fileName,FileMode.Append, FileAccess.Write, FileShare.Read))){ 
                string line;
                while((line = br.ReadLine()) != null){
                    if(line == ";") break;
                    string[] values = line.Split(' ');
                    int[] res = new int[values.Length];
                    for(int i=0; i< values.Length; ++i){
                        res[i] = Int32.Parse(values[i]);
                    }
                    String r = ToString(SelectionSort(res));
                    bw.WriteLine(r);
                    // Console.WriteLine(r);
                }
            }
        }
    }

    static int[] SelectionSort(int[] arr)
    {
        for (int i = 0; i < arr.Length - 1; i++)
        {
            int index = i;
            for (int j = i + 1; j < arr.Length; j++)
                if (arr[j] < arr[index])
                    index = j;
     
            int smallerNumber = arr[index];  
            arr[index] = arr[i];
            arr[i] = smallerNumber;
        }
        return arr;
    }
    
    static String ToString(int[] v)
    {
        String s = "";
        for(int i=0;i < v.Length; ++i){
            s+=v[i].ToString();
            if(i < v.Length-1) s+=" ";
        }
        return s;
    }
}