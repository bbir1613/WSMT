create table books(
	id int not null primary key AUTO_INCREMENT,
	author_id  int not null,
	isbn varchar(255),
	title varchar(255),
	publisher varchar(255),
	published_year int
);


create table authors(
	id int not null primary key AUTO_INCREMENT,
	first_name varchar(255),
	last_name varchar(255)
);

alter table books add constraint fk_books_authors foreign key(author_id) references authors(id) 
ON DELETE CASCADE ON UPDATE CASCADE;


insert into authors(first_name, last_name) values ('Mihai', 'Eminescu');
insert into authors(first_name, last_name) values ('Daniel', 'Tulcea');
insert into authors(first_name, last_name) values ('Magda', 'Ianos');

insert into books(author_id, isbn, title, publisher, published_year) values (2,'978-606-666-002-0','Urme in vesnicie','Doxologia', 2013);
insert into books(author_id, isbn, title, publisher, published_year) values (2,'978-606-8117-98-0','Epifania','Doxologia', 2011);
insert into books(author_id, isbn, title, publisher, published_year) values (2,'978-9732402115','Albatros','Albatros', 1991);
insert into books(author_id, isbn, title, publisher, published_year) values (1,'978-973-128-453-8','Iubire, intlelepciune fara sfirsit','Corint Junior',2014);
insert into books(author_id, isbn, title, publisher, published_year) values (3,'973-9016-42-1','Poezii','Libra',	1996);
insert into books(author_id, isbn, title, publisher, published_year) values (3,'-','Poezii','Minerva',1988);