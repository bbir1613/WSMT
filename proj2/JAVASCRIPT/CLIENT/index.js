const stompit = require('stompit');

const request = '/queue/'
const response = 'response/'
const GET_BOOKS = 'get/books'
const GET_AUTHORS = 'get/authors'
const POST_BOOK = 'post/book'
const POST_AUTHOR = 'post/author'
const PUT_BOOK = 'put/book'
const PUT_AUTHOR = 'put/author'
const DELETE_BOOK = 'delete/book'
const DELETE_AUTHOR = 'delete/authors'

class Author{
  constructor({id = undefined, first_name = undefined, last_name = undefined}){
    this.id = id
    this.first_name = first_name
    this.last_name = last_name
  }
}

class Book{
  constructor({id= undefined, author= undefined, isbn= undefined, title= undefined, 
               published_year= undefined, publisher= undefined}){
    this.id = id
    this.isbn = isbn
    this.title = title
    this.published_year = published_year
    this.publisher = publisher
    this.author = author    
  }
}

const connectCallback = (error,client)=>{
    if(error){
      console.error(` connect error `, error.message);
      return;
    }

    const on_message = (message, destination)=>{
      console.log(` on_message ` ,message, destination);
      if(destination === request+ response+ POST_BOOK){
        sendMessage(message, request+PUT_BOOK);
      }
      if(destination === request+ response+ POST_AUTHOR){
        sendMessage(message, request+PUT_AUTHOR);
      }
      if(destination === request + response + PUT_BOOK){
        sendMessage(message, request+DELETE_BOOK);
        sendMessage(JSON.stringify(JSON.parse(message).author), request+ DELETE_AUTHOR);
      }
      if(destination === request + response + PUT_AUTHOR){
        sendMessage(message, request+ DELETE_AUTHOR);
      }
    }

    const sendMessage = (body, destination)=>{
      const frame = client.send(destination);
      console.log(body, destination);
      frame.write(body);
      frame.end();
    };

    const subscribeCallback = (callback) => (error, message)=>{
      if(error){
           console.error(` subscribe error `, error.message);
           return;
      }

      message.readString('utf-8',(error, body)=>{
        if(error){
          console.error(` read message error `, error.message);
        }
        client.ack(message);
        callback(body, message.headers.destination);
      });
    }
  
    const routes = [
                {
                  destination:request+ response +GET_BOOKS,
                  callback: on_message,
                },
                {
                  destination:request+ response +GET_AUTHORS,
                  callback: on_message,   
                },
                {
                  destination:request+ response +POST_BOOK,
                  callback: on_message,
                },
                {
                  destination:request+ response +POST_AUTHOR,
                  callback: on_message,
                },
                {
                  destination:request+ response +PUT_BOOK,
                  callback: on_message,
                },
                {
                  destination:request+ response +PUT_AUTHOR,
                  callback: on_message,
                },
                {
                  destination:request+ response +DELETE_BOOK,
                  callback: on_message,
                },
                {
                  destination:request+ response +DELETE_AUTHOR,
                  callback: on_message
                }
              ];
    for(let route of routes){
      client.subscribe({'destination':route.destination, 
                        'ack':'client-individual',
                        },
                         subscribeCallback(on_message));
    }

    let what = {}
    sendMessage(JSON.stringify(what), request+ GET_BOOKS);

    sendMessage(JSON.stringify(what), request+ GET_AUTHORS);

    what = new Book({
                     id: 0, 
                     author: new Author({id: 0, first_name:"huh", last_name:"heh"}), 
                     isbn: 'huh', 
                     title: 'Test', 
                     published_year: 124, 
                     publisher: 'test'
                    });
    sendMessage(JSON.stringify(what), request+POST_BOOK);

    what = new Author({id: 0, first_name: "huh", last_name: "heh"});
    sendMessage(JSON.stringify(what), request+POST_AUTHOR);

    setTimeout(()=> client.disconnect(), 2000);
}

const connectOptions = {
  'host': 'localhost',
  'port': 61613,
  'connectHeaders':{
    'host': '/',
    'login': '',
    'passcode': '',
    'heart-beat': '5000,5000'
  }
};

stompit.connect(connectOptions,connectCallback);