const AuthorRepository = require('./src/repository/author_repository');
const BookRepository = require('./src/repository/book_repository');
const Server = require('./src/net');
const Service = require('./src/service');

const authorRepository = new AuthorRepository();
const bookRepository = new BookRepository();
const service = new Service(authorRepository, bookRepository);
const server = new Server(service.get_routes());
server.start()