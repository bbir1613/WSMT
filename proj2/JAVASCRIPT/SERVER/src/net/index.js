const stompit = require('stompit');

class Server{
  constructor(routes = []){
    this.routes = routes;
    this.connectOptions = {
                            'host': 'localhost',
                            'port': 61613,
                            'connectHeaders':{
                              'host': '/',
                              'login': '',
                              'passcode': '',
                              'heart-beat': '5000,5000'
                            }
                          };  
    this.connectCallback.bind(this);
    this.start.bind(this);
  }

  connectCallback (error,client){
    if(error){
      console.error(` connect error `, error.message);
      return;
    }

    const sendMessage = (body, destination)=>{
      const frame = client.send(destination);
      console.log(destination , body);
      frame.write(body);
      frame.end();
    };

    const subscribeCallback = (callback) => (error, message)=>{
      if(error){
           console.error(` subscribe error `, error.message);
           return;
      }
      message.readString('utf-8',(error, body)=>{
        if(error){
          console.error(` read message error `, error.message);
        }
        //console.log(` receive the message `, body);
        client.ack(message);
        callback(body).then(({destination, response})=> sendMessage(response,destination));
      });
    }
    for(let route of this.routes){
      client.subscribe({'destination':route.destination, 
                        'ack':'client-individual',
                        },
                         subscribeCallback(route.callback));
    }
    
    // client.disconnect();
  };

  start(){
    stompit.connect(this.connectOptions,this.connectCallback.bind(this));
  }
}

module.exports = Server;