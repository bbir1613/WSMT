const mysql = require('mysql');

const execute = (query, values = undefined)=>{
	TAG = `execute_query`;
	Log(TAG, "query : ", query, " values: ", values);
	const cnx = get_database_connection();
	return new Promise((resolve,reject)=> cnx.connect(err=> err? reject(err):
		cnx.query(query,values,(err,result,fields)=>{
			if(err)reject(err);
			cnx.end();
			resolve(JSON.parse(JSON.stringify(result)));	
		})));
};

const get_database_connection = ()=>mysql.createConnection({
											  host: "localhost",
											  user: "root",
											  password: "",
											  database: "proj2"
											});

// const Log = (TAG, ...message)=> console.log(TAG,'\t', message);
const Log = (TAG, ...message)=> console.log('');

module.exports = {execute:execute};