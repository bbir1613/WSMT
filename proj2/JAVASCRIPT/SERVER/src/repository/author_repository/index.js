const db = require('../../util');
const Author = require('../../model/author');
const AUTHOR_TABLE = "authors"

class AuthorRepository{

	constructor(){
		this.create.bind(this);
		this.update.bind(this);
		this.delete.bind(this);
		this.find_all_authors.bind(this);
		this.find_author_by_name.bind(this);
		this.run.bind(this);
	}

	create(author){
		console.log(` create ` ,author)
		const query = ` INSERT INTO ${AUTHOR_TABLE}(first_name, last_name) `+ 
					  ` VALUES('${author.first_name}','${author.last_name}') `;
		return db.execute(query).then(res=>{
			author = {...author, id: res.insertId}
			return author;
		})
	}

	update(author){
		console.log(` update ` ,author)
		const query = ` UPDATE ${AUTHOR_TABLE} SET `+ 
					  ` first_name = '%s' , `+
					  ` last_name = '%s' `+
					  ` WHERE id = '%s'  `
		return db.execute(query).then(res=> true);
	}

	delete(author){
		console.log(` delete `,author);
		const query = ` DELETE from ${AUTHOR_TABLE} WHERE id = '${author.id}' `;
		return db.execute(query).then(res=> true);
	}

	find_all_authors(){
		const query = ` SELECT id , first_name, last_name from ${AUTHOR_TABLE} `;
		return db.execute(query);
	}

	find_author_by_name( first_name, last_name){
		const query = ` SELECT id, first_name, last_name from ${AUTHOR_TABLE} `+ 
					  ` where first_name = '${first_name}' and last_name = '${last_name}' `;
		return db.execute(query).then(res=>{
			if(res.length === 0 ) return undefined
			return res[0];
		});
	}

	run(){
		console.log("Hello world AuthorRepository repository")	
	}
	
}

module.exports = AuthorRepository;