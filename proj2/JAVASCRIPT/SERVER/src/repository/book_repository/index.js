const db = require('../../util');
const Book = require('../../model/book');
const Author = require('../../model/author');

const BOOK_TABLE = "books";
const AUTHOR_TABLE = "authors";

class BookRepository{

	constructor(){
		this.create.bind(this);
		this.update.bind(this);
		this.delete.bind(this);
		this.find_all_books.bind(this);
	}

	create(book){
		console.log(book)
		const query = ` INSERT INTO ${BOOK_TABLE}(author_id, isbn, title, publisher, published_year)`+
					  ` VALUES('${book.author.id}', '${book.isbn}', '${book.title}', '${book.publisher}', '${book.published_year}') `;
		console.log(query)
		return db.execute(query).then(res=>{
			book = {...book , id:res.insertId};
			return book;
		});
	}

	update(book){
		console.log(book)
		const query = ` UPDATE ${BOOK_TABLE} set `+
					  ` isbn = '${book.isbn}', `+
					  ` title = '${book.title}', `+
					  ` publisher= '${book.publisher}', `+
					  ` published_year = '${book.published_year}' `+
					  ` where id = '${book.id}' `;
		return db.execute(query);
	}

	delete(book){
		console.log(book)
		const query = ` DELETE from ${BOOK_TABLE} `+
					  ` where id = '${book.id}' `;
		return db.execute(query).then(res => true);
	}

	find_all_books(){
		const query = ` SELECT book.id, author_id, first_name, last_name , isbn, title, publisher, published_year `+
					  ` from ${BOOK_TABLE} as book inner join ${AUTHOR_TABLE} as author `+
					  ` where author_id = author.id `;
		return db.execute(query);					   
	}

	run(){
		console.log("Hell world from BookRepository");
	}
}

module.exports = BookRepository;