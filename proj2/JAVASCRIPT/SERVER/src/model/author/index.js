class Author{

  constructor({id = undefined, first_name = undefined, last_name = undefined}){
    this.id = id
    this.first_name = first_name
    this.last_name = last_name
  }

}

module.exports = Author;