class Book{

  constructor({id= undefined, author= undefined, isbn= undefined, title= undefined, 
               published_year= undefined, publisher= undefined}){
    this.id = id
    this.isbn = isbn
    this.title = title
    this.published_year = published_year
    this.publisher = publisher
    this.author = author    
  }
  
}

module.exports = Book;