const Book = require('../model/book');
const Author = require('../model/author');

const request = '/queue/'
const response = 'response/'
const GET_BOOKS = 'get/books'
const GET_AUTHORS = 'get/authors'
const POST_BOOK = 'post/book'
const POST_AUTHOR = 'post/author'
const PUT_BOOK = 'put/book'
const PUT_AUTHOR = 'put/author'
const DELETE_BOOK = 'delete/book'
const DELETE_AUTHOR = 'delete/authors'

class Service{
	
	constructor(authorRepository, bookRepository){
		this.authorRepository = authorRepository; 
		this.bookRepository = bookRepository;
		//binds
		this.get_books.bind(this);
		this.get_authors.bind(this);
		this.post_book.bind(this);
		this.post_author.bind(this);
		this.put_book.bind(this);
		this.put_author.bind(this);
		this.delete_book.bind(this);
		this.delete_author.bind(this);
	}

	get_books(message){
	    console.log('get_books ' ,message)
	    return this.bookRepository.find_all_books().then(books=>({
	      destination: request+response+GET_BOOKS,
	      response: JSON.stringify(books)
	      }));
	}

	get_authors(message){
	    console.log('get_authors ' ,message)
	    return this.authorRepository.find_all_authors().then(authors=>({
	      destination:request+ response+ GET_AUTHORS,   
	      response: JSON.stringify(authors)	    	
	    }));
	}
	
	post_book(message){
	    console.log('post_book ' ,message);
	    const book = new Book({...JSON.parse(message)});
	    const createBook = (author)=>{
	    	book.author = author;
	    	return this.bookRepository.create(book).then(book=>({
	    		destination: request+ response+ POST_BOOK,
	    	    response: JSON.stringify(book)
	    	}));
	    }
	    return this.authorRepository.find_author_by_name(book.author.first_name, book.author.last_name).then(author=>{
	    	if(author) return createBook(author);
	    	return this.authorRepository.create(book.author).then(author=> createBook(author));
	    });
	}

	post_author(message){
	    console.log('post_author ' ,message)
	    console.log(` POST AUTHOR`, Author);
	    const author = new Author({...JSON.parse(message)});
	    return this.authorRepository.create(author).then(author=>({
	      destination:request+ response+ POST_AUTHOR,
	      response: JSON.stringify(author)
	  }));
	}

	put_book(message){
	    console.log('put_book ' ,message)
	   	const book = new Book({...JSON.parse(message)});
	   	return this.authorRepository.update(book.author).then(res=>{
	   		return this.bookRepository.update(book).then(res=>({
	   			destination:request+ response+ PUT_BOOK,
	      		response: JSON.stringify(book)
	   		}));
	   	});
	}

	put_author(message){
	    console.log('put_author ' ,message);
	    const author = new Author({...JSON.parse(message)});
	    return this.authorRepository.update(author).then(res=>({
	    	destination:request+ response+ PUT_AUTHOR,
	      	response: JSON.stringify(author)
	    }));
	}

	delete_book(message){
	    console.log('delete_book ' ,message)
	    const book = new Book({...JSON.parse(message)});
	    return this.bookRepository.delete(book).then(res=>({
	    	destination:request+ response+ DELETE_BOOK,
	      	response: JSON.stringify({'status': res})
	    }))
	}

	delete_author(message){
	    console.log('delete_author ' ,message)
	    const author = new Author({...JSON.parse(message)});
	    return this.authorRepository.delete(author).then(res=>({
	      destination:request+ response+ DELETE_AUTHOR,
	      response: JSON.stringify({status: res})	    	
	    }));
	}
	
	get_routes(){   
	  const routes = [
	            {
	              destination:request+ GET_BOOKS,
	              callback: this.get_books.bind(this),
	            },
	            {
	              destination:request+ GET_AUTHORS,
	              callback: this.get_authors.bind(this),   
	            },
	            {
	              destination:request+ POST_BOOK,
	              callback: this.post_book.bind(this),
	            },
	            {
	              destination:request+ POST_AUTHOR,
	              callback: this.post_author.bind(this),
	            },
	            {
	              destination:request+ PUT_BOOK,
	              callback: this.put_book.bind(this),
	            },
	            {
	              destination:request+ PUT_AUTHOR,
	              callback: this.put_author.bind(this),
	            },
	            {
	              destination:request+ DELETE_BOOK,
	              callback: this.delete_book.bind(this),
	            },
	            {
	              destination:request+ DELETE_AUTHOR,
	              callback: this.delete_author.bind(this)
	            }
	          ]
	    return routes
	}

}

module.exports = Service;