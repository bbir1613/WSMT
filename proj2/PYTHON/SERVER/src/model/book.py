from model.author import Author
import json

class Book:
	def __init__(self, id= None, author= None, isbn= None, title= None, publisher= None, published_year= None):
		self.id = id
		self.isbn = isbn
		self.title = title
		self.published_year = published_year
		self.publisher = publisher
		self.author = author

	def __repr__(self):
		return '%s' % (self.to_json())
	
	def from_json(self, json):
		if json != None:
			self.id = json.get('id')
			self.isbn = json.get('isbn')
			self.title = json.get('title')
			self.published_year = json.get('published_year')
			self.publisher = json.get('publisher')
			self.author =Author().from_json(json.get('author'))
		return self
		
	def to_json(self):
		author = self.author;
		if isinstance(author, Author):
			author = self.author.to_json()

		return {
				'id':self.id,
				'isbn':self.isbn,
				'title':self.title,
				'published_year':self.published_year,
				'publisher':self.publisher,
				'author':author
				}