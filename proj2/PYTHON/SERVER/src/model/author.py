class Author:
	def __init__(self, id = None, first_name = None, last_name = None):
		self.id = id
		self.first_name = first_name
		self.last_name = last_name

	def __repr__(self):
		return '%s' % (self.to_json())

	def from_json(self,json):
		if json != None:
			self.id = json.get('id')
			self.first_name = json.get('first_name')
			self.last_name = json.get('last_name')
		return self

	def to_json(self):
		return {
				'id':self.id,
				'first_name':self.first_name,
				'last_name':self.last_name
				}