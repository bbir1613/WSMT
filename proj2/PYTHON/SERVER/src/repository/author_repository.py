import util.util as db
from pony.orm import *
from model.author import Author 
from entity.author_entity import AuthorEntity 

AUTHOR_TABLE = "authors"

class AuthorRepository:
	def __init__(self):
		pass

	@db_session()
	def create(self,author):	
		authorEntity = AuthorEntity(first_name = author.first_name, last_name = author.last_name)
		flush()
		author.id = authorEntity.id
		return author

	@db_session()
	def update(self, author):
		print(author)
		authorEntity = AuthorEntity[author.id]
		authorEntity.first_name = author.first_name
		authorEntity.last_name = author.last_name
		flush()
		return True

	@db_session()
	def delete(self, author):
		print(author);
		authorEntity = AuthorEntity[author.id]
		authorEntity.delete()
		flush()
		return True

	@db_session()
	def find_all_authors(self):
		authorEntities = AuthorEntity.select()[:]
		return [Author(authorEntity.id, authorEntity.first_name, authorEntity.last_name) 
				for authorEntity in authorEntities]

	@db_session()
	def find_author_by_name(self, first_name, last_name):
		authors = self.find_all_authors()
		for author in authors:
			if author.first_name == first_name and author.last_name == last_name:
				return author
		return None

	def run(self):
		print("Hello world AuthorRepository repository")	