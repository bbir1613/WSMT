import util.util as db
from model.book import Book 
from model.author import Author
from entity.book_entity import BookEntity
from entity.author_entity import AuthorEntity
from pony.orm import *

BOOK_TABLE = "books"
AUTHOR_TABLE = "authors"

class BookRepository:
	def __init__(self):
		pass

	@db_session()
	def create(self, book):
		print(book)
		bookEntity = BookEntity(isbn = book.isbn, 
								title = book.title, 
								published_year = book.published_year, 
								publisher = book.publisher, 
								author = AuthorEntity[book.author.id])
		flush()
		book.id = bookEntity.id
		return book

	@db_session()
	def update(self, book):
		print(book)
		bookEntity = BookEntity[book.id]
		bookEntity.isbn = book.isbn
		bookEntity.title = book.title
		bookEntity.published_year = book.published_year
		bookEntity.publisher = book.publisher
		flush()
		return True

	@db_session()
	def delete(self,book):
		print(book)
		bookEntity = BookEntity[book.id]
		bookEntity.delete()
		flush()
		return True

	@db_session()
	def find_all_books(self):
		return [Book(bookEntity.id, 
					 Author(bookEntity.author.id, bookEntity.author.first_name, bookEntity.author.last_name),
					 bookEntity.isbn,
					 bookEntity.title,
					 bookEntity.publisher,
					 bookEntity.published_year					 
					 ) for bookEntity in BookEntity.select()[:]]