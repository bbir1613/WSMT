from util.database import db 
from pony.orm import *
from entity.author_entity import AuthorEntity;
import json

class BookEntity(db.Entity):

	isbn = Required(str)
	title = Required(str)
	published_year = Required(int)
	publisher = Required(str)
	author = Required(AuthorEntity)