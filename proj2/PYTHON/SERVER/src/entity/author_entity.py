from util.database import db 
from pony.orm import *
# from entity.book_entity import BookEntity;

class AuthorEntity(db.Entity):

	id = PrimaryKey(int, auto=True)
	first_name = Required(str)
	last_name = Required(str)
	books = Set('BookEntity')

	def get_id(self):
		return self.id
