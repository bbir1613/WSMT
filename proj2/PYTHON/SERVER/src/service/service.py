'''
Biblioteca: Evidenta carti, autori:
* Introduceri / stergeri / modificari carti si autori
* Lista cartilor cu filtre dupa parti din titlu
* Lista autorilor cu filtre dupa parti din nume
'''
import json
from model.book import Book
from model.author import Author 

request = '/queue/'
response = 'response/'
GET_BOOKS = 'get/books'
GET_AUTHORS = 'get/authors'
POST_BOOK = 'post/book'
POST_AUTHOR = 'post/author'
PUT_BOOK = 'put/book'
PUT_AUTHOR = 'put/author'
DELETE_BOOK = 'delete/book'
DELETE_AUTHOR = 'delete/authors'

class Service():
	def __init__(self, book_repository, author_repository):
		self.book_repository = book_repository 
		self.author_repository = author_repository

	def get_books(self, message):
		print('get_books %s' %message)
		books = self.book_repository.find_all_books();
		return (
			request+response+GET_BOOKS,
			json.dumps([book.to_json() for book in books])
			)

	def get_authors(self, message):
		print('get_authors %s' %message)
		authors = self.author_repository.find_all_authors()
		return(
			request+ response+ GET_AUTHORS,		
			json.dumps([author.to_json() for author in authors])
			)

	def post_book(self, message):
		print('post_book %s' %message)
		book = Book().from_json(message)
		author = self.author_repository.find_author_by_name(book.author.first_name, book.author.last_name)
		if author == None:
			author = self.author_repository.create(book.author)
		book.author = author
		book = self.book_repository.create(book)
		return(
			request+ response+ POST_BOOK,
			json.dumps(book.to_json())
			)

	def post_author(self, message):
		print('post_author %s' %message)
		author = self.author_repository.create(Author().from_json(message))
		return(
			request+ response+ POST_AUTHOR,
			json.dumps(author.to_json())
			)

	def put_book(self, message):
		print('put_book %s' %message)
		book = Book().from_json(message);
		self.author_repository.update(book.author)
		self.book_repository.update(book)
		return(
			request+ response+ PUT_BOOK,
			json.dumps(book.to_json())
			)

	def put_author(self, message):
		print('put_author %s' %message)
		author = Author().from_json(message)
		self.author_repository.update(author)
		return(
			request+ response+ PUT_AUTHOR,
			json.dumps(author.to_json())
			)

	def delete_book(self, message):
		print('delete_book %s' %message)
		status = self.book_repository.delete(Book().from_json(message))
		return(
			request+ response+ DELETE_BOOK,
			json.dumps({'status':status})
			)

	def delete_author(self, message):
		print('delete_author %s' %message)
		status = self.author_repository.delete(Author().from_json(message))
		return(
			request+ response+ DELETE_AUTHOR,
			json.dumps({'status': status})
			)

	def get_routes(self):
		routes = {
			request+ GET_BOOKS: 	self.get_books,
			request+ GET_AUTHORS: 	self.get_authors,		
			request+ POST_BOOK: 	self.post_book,
			request+ POST_AUTHOR:	self.post_author,
			request+ PUT_BOOK: 		self.put_book,
			request+ PUT_AUTHOR: 	self.put_author,
			request+ DELETE_BOOK: 	self.delete_book,
			request+ DELETE_AUTHOR: self.delete_author
		}
		return routes