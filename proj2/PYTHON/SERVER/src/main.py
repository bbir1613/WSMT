from net.server import Server
from service.service import Service
from repository.book_repository import BookRepository
from repository.author_repository import AuthorRepository
from model.author import Author;
from model.book import Book;
from entity.author_entity import AuthorEntity;
from entity.book_entity import BookEntity;
from util.database import db;
from pony.orm import *
import json

@db_session()
def insert_into_db(author_repository, book_repository):
	
	a1 = author_repository.create(Author(0, "Mihai", "Eminescu"));
	a2 = author_repository.create(Author(0, "Daniel", "Tulcea"));
	a3 =author_repository.create(Author(0, "Magda", "Ianos"));

	authorEntity = AuthorEntity[a2.id]
	bookEntity = Book(None, authorEntity, "978-606-666-002-0", "Urme in vesnicie", "Doxologia",  2013);
	book_repository.create(bookEntity);

	bookEntity = Book(None, authorEntity, "978-606-8117-98-0", "Epifania", "Doxologia", 2011);
	book_repository.create(bookEntity);

	bookEntity = Book(None, authorEntity, "978-9732402115", "Albatros", "Albatros",1991);
	book_repository.create(bookEntity);

	authorEntity = AuthorEntity[a1.id]
	bookEntity = Book(None, authorEntity, "978-973-128-453-8", "Iubire, intlelepciune fara sfirsit", "Corint Junior", 2014);
	book_repository.create(bookEntity);

	authorEntity = AuthorEntity[a3.id];
	bookEntity = Book(None, authorEntity, "973-9016-42-1", "Poezii", "Libra", 1996);
	book_repository.create(bookEntity);

	bookEntity = Book(None, authorEntity, "-","Poezii", "Minerva", 1988);
	book_repository.create(bookEntity);



if __name__ == "__main__":
	db.generate_mapping(create_tables=True)
	db.drop_all_tables(with_all_data=True)
	db.create_tables()
	book_repository = BookRepository()
	author_repository = AuthorRepository()
	insert_into_db(author_repository, book_repository)
	service = Service(book_repository, author_repository)
	server = Server(service.get_routes());
	server.start();

'''

	response = None;

	# response = service.get_books('{}')
	# print('\n get_books : ', response)
	# response = service.get_authors('{}')
	# print('\n get_authors: ', response)
	# author = Author(id = None, authorEntity, first_name = "Author first name ", last_name = "Author last name ")
	# response = service.post_author(author.to_json())
	# author = Author().from_json(json.loads(response[1]));
	# print('\n post_author: ', response)
	# book = Book(None, authorEntity, id= None, authorEntity, author= author, isbn= 'huh', title= 'Test', published_year= 124, publisher= 'test')
	# response = service.post_book(book.to_json())
	# response = service.post_book(book.to_json())
	# book = Book(None, authorEntity, ).from_json(json.loads(response[1]))
	# print('\n post_book: ', response)
	# book.isbn = "test update"
	# book.author.last_name = "updated from book"
	# author.first_name = "updated from author"
	# response = service.put_book(book.to_json())
	# print('\n put_book: ', response)
	# response = service.put_author(author.to_json())
	# print('\n put_author: ', response)
	# response = service.delete_book(book.to_json())
	# print('\n delete_book: ', response)
	# response = service.delete_author(author.to_json())
	# print('\n delete_author: ', response)

'''