import stomp

class MessageListener(stomp.ConnectionListener):
	def __init__(self, queue):
		self.__queue = queue

	def on_message(self,headers,message):
		self.__queue.put((headers["destination"],message))
