import stomp
import queue
import json
from net.listener.message_listener import MessageListener 
from net.listener.debugger import Debugger 

class Server():
	def __init__(self, routes):
		self.___routes = routes
		self.__conn = stomp.Connection()
		self.__queue = queue.Queue()
		self.__conn.set_listener('',MessageListener(self.__queue))
		# self.__conn.set_listener('debugger',Debugger())

	def start(self):
		print("connecting to stomp")
		self.__conn.start()
		self.__conn.connect('', '', wait=True)
		id = 1;
		for path in self.___routes.keys():
			self.__conn.subscribe(destination = path, id=id, ack='auto')
			id+=1
		print("connection established")
		while True:
			request = self.__queue.get();
			self.execute(request[0],request[1])
		self.__conn.close()

	def execute(self,route,message):
		try:
			response = self.___routes[route](json.loads(message));
			if response != None and len(response) == 2:
				# print(response)
				self.__conn.send(destination = response[0] , body = response[1])
		except Exception :
			pass