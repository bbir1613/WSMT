import time
import sys
import stomp
import json

request = '/queue/'
response = 'response/'
GET_BOOKS = 'get/books'
GET_AUTHORS = 'get/authors'
POST_BOOK = 'post/book'
POST_AUTHOR = 'post/author'
PUT_BOOK = 'put/book'
PUT_AUTHOR = 'put/author'
DELETE_BOOK = 'delete/book'
DELETE_AUTHOR = 'delete/authors'

class Author:
	def __init__(self, id = None, first_name = None, last_name = None):
		self.id = id
		self.first_name = first_name
		self.last_name = last_name

	def __repr__(self):
		return '%s' % (self.to_json())

	def from_json(self,json):
		if json != None:
			self.id = json.get('id')
			self.first_name = json.get('first_name')
			self.last_name = json.get('last_name')
		return self

	def to_json(self):
		return {
				'id':self.id,
				'first_name':self.first_name,
				'last_name':self.last_name
				}

class Book:
	def __init__(self, id= None, author= None, isbn= None, title= None, published_year= None, publisher= None):
		self.id = id
		self.isbn = isbn
		self.title = title
		self.published_year = published_year
		self.publisher = publisher
		self.author = author

	def __repr__(self):
		return '%s' % (self.to_json())
	
	def from_json(self, json):
		if json != None:
			self.id = json.get('id')
			self.isbn = json.get('isbn')
			self.title = json.get('title')
			self.published_year = json.get('published_year')
			self.publisher = json.get('publisher')
			self.author =Author().from_json(json.get('author'))
		return self
		
	def to_json(self):
		author = self.author;
		if isinstance(author, Author):
			author = self.author.to_json()

		return {
				'id':self.id,
				'isbn':self.isbn,
				'title':self.title,
				'published_year':self.published_year,
				'publisher':self.publisher,
				'author':author
				}

class MyListener(stomp.ConnectionListener):
    def __init__(self, conn):
    	self.conn = conn 

    def on_error(self, headers, message):
    	print('received an error "%s"' % message)

    def on_message(self, headers, message):
        if headers["destination"] == request+ response+ POST_BOOK:
        	self.conn.send(body = message, destination = request + PUT_BOOK)
        if headers["destination"] == request+ response+ POST_AUTHOR:
        	self.conn.send(body = message, destination = request + PUT_AUTHOR)
        if headers["destination"] == request + response + PUT_BOOK:
        	self.conn.send(body = message, destination = request + DELETE_BOOK)
        	author = json.loads(message)["author"];
        	self.conn.send(body = json.dumps(author), destination = request + DELETE_AUTHOR)
        if headers["destination"] == request + response + PUT_AUTHOR:
        	self.conn.send(body = message, destination = request + DELETE_AUTHOR)

        print(headers, message)

if __name__ == "__main__":
	conn = stomp.Connection()
	conn.set_listener('/test', MyListener(conn))
	conn.start()
	conn.connect('', '', wait=True)

	routes = [
			request+ response+ GET_BOOKS ,
			request+ response+ GET_AUTHORS ,		
			request+ response+ POST_BOOK,
			request+ response+ POST_AUTHOR,
			request+ response+ PUT_BOOK,
			request+ response+ PUT_AUTHOR,
			request+ response+ DELETE_BOOK,
			request+ response+ DELETE_AUTHOR
	]

	id = 1;
	for path in routes:
		conn.subscribe(destination=path, id=id, ack='auto')
		id+=1
		
	what = {} 
	conn.send(body = json.dumps(what), destination = request+ GET_BOOKS)

	what = {}
	conn.send(body = json.dumps(what), destination = request+ GET_AUTHORS)

	what = Book(id= 0, author= Author(id = 0, first_name = "huh", last_name ="heh"), 
				isbn= 'huh', title= 'Test', published_year= 124, publisher= 'test')
	conn.send(body = json.dumps(what.to_json()), destination = request+ POST_BOOK)
	what = Author(id = 0, first_name = "huh", last_name ="heh")
	conn.send(body = json.dumps(what.to_json()), destination = request+ POST_AUTHOR)
	
	time.sleep(2)
	conn.disconnect()