import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQBytesMessage;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.apache.activemq.util.ByteSequence;
import com.google.gson.*;

public class Client {
	
	private static String url = ActiveMQConnection.DEFAULT_BROKER_URL;

	public static class Author{
		public int id;
		public String first_name;
		public String last_name;

		public Author(){

		}

		public Author(int id, String first_name, String last_name){
			this.id = id;
			this.first_name = first_name;
			this.last_name = last_name;
		}

		public String toString(){
			return new Gson().toJson(this);
		}
	}

	public static class Book{
		public int id;
		public String isbn;
		public String title;
		public int published_year;
		public String publisher;
		public Author author;

		public Book(){

		}

		public Book(int id, String isbn, String title, int published_year, String publisher, Author author){
			this.id = id; 
			this.isbn = isbn;
			this.title = title;
			this.published_year = published_year; 
			this.publisher = publisher;
			this.author = author;
		}

		public String toString(){
			return new Gson().toJson(this);
		}
	}

	public static <T> T readMessage(MessageConsumer consumer, Class<T> clazz) throws JMSException {
		Message message = consumer.receive();
		String str = null;
		if(message instanceof ActiveMQBytesMessage){
			str = new String(((ActiveMQBytesMessage)message).getContent().getData());
		}else if( message instanceof ActiveMQTextMessage){
			str = ((ActiveMQTextMessage) message).getText();
		}
		System.out.println(str);
		if(clazz == null) return null;
		return new Gson().fromJson(str, clazz);
	}

	public static String response = "response/";
	public static String GET_BOOKS = "get/books";
	public static String GET_AUTHORS = "get/authors";
	public static String POST_BOOK = "post/book";
	public static String POST_AUTHOR = "post/author";
	public static String PUT_BOOK = "put/book";
	public static String PUT_AUTHOR = "put/author";
	public static String DELETE_BOOK = "delete/book";
	public static String DELETE_AUTHOR = "delete/authors";

	public static void main(String[] args) throws JMSException {
	    ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);
	    Destination destination = null;
	    MessageProducer producer = null;
		MessageConsumer consumer = null;
		ActiveMQBytesMessage message = null;

		{
		    Connection connection = connectionFactory.createConnection();
		    connection.start();
		    Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE); 
		    //SEND
			System.out.println(" GET AUTHORS ");
			destination = session.createQueue(GET_AUTHORS);
			producer = session.createProducer(destination);
			message = new ActiveMQBytesMessage();
			message.setContent(new ByteSequence("{}".getBytes()));
			producer.send(message);
			//RECEIVE
			destination = session.createQueue(response+GET_AUTHORS);
			consumer = session.createConsumer(destination);
			Author[] authors = readMessage(consumer, Author[].class);
			connection.close();
		}
		
		{
		    Connection connection = connectionFactory.createConnection();
		    connection.start();
		    Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE); 
		    //SEND
			System.out.println(" GET BOOKS ");
			destination = session.createQueue(GET_BOOKS);
			producer = session.createProducer(destination);
			message = new ActiveMQBytesMessage();
			message.setContent(new ByteSequence("{}".getBytes()));
			producer.send(message);
			//RECEIVE
			destination = session.createQueue(response+GET_BOOKS);
			consumer = session.createConsumer(destination);
			Book[] books = readMessage(consumer, Book[].class);
		    connection.close();
		}
		
		{
		    Connection connection = connectionFactory.createConnection();
		    connection.start();
		    Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE); 

			System.out.println(" POST BOOK ");
			//SEND
			destination = session.createQueue(POST_BOOK);
			producer = session.createProducer(destination);
			message = new ActiveMQBytesMessage();
			Book postBook = new Book(0, "isbn", "title", 1924, "asd", new Author(0, "FirstName", "LastName"));
			message.setContent(new ByteSequence(postBook.toString().getBytes()));
			producer.send(message);
			//RECEIVE
			destination = session.createQueue(response+POST_BOOK);
			consumer = session.createConsumer(destination);
			Book book = readMessage(consumer, Book.class);
			//SEND
			destination = session.createQueue(DELETE_BOOK);
			producer = session.createProducer(destination);
			message = new ActiveMQBytesMessage();
			message.setContent(new ByteSequence(book.toString().getBytes()));
			producer.send(message);
			//RECEIVE
			destination = session.createQueue(response+DELETE_BOOK);
			consumer = session.createConsumer(destination);
			readMessage(consumer, null);
		    //SEND
			destination = session.createQueue(DELETE_AUTHOR);
			producer = session.createProducer(destination);
			message = new ActiveMQBytesMessage();
			message.setContent(new ByteSequence(book.author.toString().getBytes()));
			producer.send(message);
			//RECEIVE
			destination = session.createQueue(response+DELETE_AUTHOR);
			consumer = session.createConsumer(destination);
			readMessage(consumer, null);
		    connection.close();
		}
		    
		{
		    Connection connection = connectionFactory.createConnection();
		    connection.start();
		    Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE); 

			System.out.println(" POST AUTHOR ");
			//SEND
			destination = session.createQueue(POST_AUTHOR);
			producer = session.createProducer(destination);
			message = new ActiveMQBytesMessage();
			Author postAuthor = new Author(0, "FirstName", "LastName");
			message.setContent(new ByteSequence(postAuthor.toString().getBytes()));
			producer.send(message);
			//RECEIVE
			destination = session.createQueue(response+POST_AUTHOR);
			consumer = session.createConsumer(destination);
			Author author = readMessage(consumer, Author.class);
			//SEND
			destination = session.createQueue(DELETE_AUTHOR);
			producer = session.createProducer(destination);
			message = new ActiveMQBytesMessage();
			message.setContent(new ByteSequence(author.toString().getBytes()));
			producer.send(message);
			//RECEIVE
			destination = session.createQueue(response+DELETE_AUTHOR);
			consumer = session.createConsumer(destination);
			readMessage(consumer, null);
		    connection.close();
		}

		{
		    Connection connection = connectionFactory.createConnection();
		    connection.start();
		    Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE); 

			System.out.println(" PUT BOOK ");
			// SEND
			destination = session.createQueue(POST_BOOK);
			producer = session.createProducer(destination);
			message = new ActiveMQBytesMessage();
			Book postBook = new Book(0, "isbn", "title", 1924, "asd", new Author(0, "FirstName", "LastName"));
			message.setContent(new ByteSequence(postBook.toString().getBytes()));
			producer.send(message);
			//RECEIVE
			destination = session.createQueue(response+POST_BOOK);
			consumer = session.createConsumer(destination);
			Book book = readMessage(consumer, Book.class);
			//SEND
			destination = session.createQueue(PUT_BOOK);
			producer = session.createProducer(destination);
			message = new ActiveMQBytesMessage();
			book.title = "title1";
			message.setContent(new ByteSequence(book.toString().getBytes()));
			producer.send(message);
			//RECEIVE
			destination = session.createQueue(response+PUT_BOOK);
			consumer = session.createConsumer(destination);
			book = readMessage(consumer, Book.class);
			//SEND
			destination = session.createQueue(DELETE_BOOK);
			producer = session.createProducer(destination);
			message = new ActiveMQBytesMessage();
			message.setContent(new ByteSequence(book.toString().getBytes()));
			producer.send(message);
			//RECEIVE
			destination = session.createQueue(response+DELETE_BOOK);
			consumer = session.createConsumer(destination);
			readMessage(consumer, null);
		    //SEND
			destination = session.createQueue(DELETE_AUTHOR);
			producer = session.createProducer(destination);
			message = new ActiveMQBytesMessage();
			message.setContent(new ByteSequence(book.author.toString().getBytes()));
			producer.send(message);
			//RECEIVE
			destination = session.createQueue(response+DELETE_AUTHOR);
			consumer = session.createConsumer(destination);
			readMessage(consumer, null);
		    connection.close();
		}

		{
		    Connection connection = connectionFactory.createConnection();
		    connection.start();
		    Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE); 

			System.out.println(" PUT AUTHOR ");
			//SEND
			destination = session.createQueue(POST_AUTHOR);
			producer = session.createProducer(destination);
			message = new ActiveMQBytesMessage();
			Author postAuthor = new Author(0, "FirstName", "LastName");
			message.setContent(new ByteSequence(postAuthor.toString().getBytes()));
			producer.send(message);
			//RECEIVE
			destination = session.createQueue(response+POST_AUTHOR);
			consumer = session.createConsumer(destination);
			Author author = readMessage(consumer, Author.class);
			//SEND
			destination = session.createQueue(PUT_AUTHOR);
			producer = session.createProducer(destination);
			message = new ActiveMQBytesMessage();
			author.first_name = "modified";
			message.setContent(new ByteSequence(author.toString().getBytes()));
			producer.send(message);			
			//RECEIVE
			destination = session.createQueue(response+PUT_AUTHOR);
			consumer = session.createConsumer(destination);
			author = readMessage(consumer, Author.class);
			//SEND
			destination = session.createQueue(DELETE_AUTHOR);
			producer = session.createProducer(destination);
			message = new ActiveMQBytesMessage();
			message.setContent(new ByteSequence(author.toString().getBytes()));
			producer.send(message);
			//RECEIVE
			destination = session.createQueue(response+DELETE_AUTHOR);
			consumer = session.createConsumer(destination);
			readMessage(consumer, null);
		    connection.close();
		}

	}
}