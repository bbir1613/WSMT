package net;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQBytesMessage;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.apache.activemq.util.ByteSequence;
import util.HandleRequest;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.lang.Thread;

public class Server{

	private static String url = ActiveMQConnection.DEFAULT_BROKER_URL;
	private static String response = "response/";
	private Map<String, HandleRequest> handler;
	private List<Thread> threads;

	public Server(Map<String, HandleRequest> handler){
		this.handler = handler;	
		this.threads = new ArrayList<Thread>();
	}
	
	public void run() throws JMSException{
		System.out.println("Server Hello world ");
	    ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);
	    Connection connection = connectionFactory.createConnection();
	    connection.start();
	    final Session session = connection.createSession(false,Session.AUTO_ACKNOWLEDGE); 
		for(final String route : handler.keySet()){
			System.out.println(route);
			Thread th = new Thread(()->handleThread(session, route, handler.get(route)));
			threads.add(th);
			th.start();
		}
	    // connection.stop();
	}

	private void handleThread(Session session, String route, HandleRequest handle){
		try{
			Destination read = session.createQueue(route);
			Destination send = session.createQueue(response + route);
			MessageConsumer consumer = session.createConsumer(read);
		    MessageProducer producer = session.createProducer(send);
		    while(true){
		    	try{
			    	Message message = consumer.receive();
					String str = null;
					if(message instanceof ActiveMQBytesMessage){
						str = new String(((ActiveMQBytesMessage)message).getContent().getData());
					}else if( message instanceof ActiveMQTextMessage){
						str = ((ActiveMQTextMessage) message).getText();
					}
					String result = handle.handle(str);
					ActiveMQBytesMessage m = new ActiveMQBytesMessage();
					m.setContent(new ByteSequence(result.getBytes()));
					producer.send(m);
				}catch(Exception ex){
					System.out.println("Error in la call  " + ex.toString());
				}
		    }
		}catch(JMSException ex){
			//ignore
			System.out.println("Error in handle thread  " + ex.toString());
		}		    
	}
}