package util;
import java.lang.FunctionalInterface;

@FunctionalInterface
public interface HandleRequest{
	public String handle(String message);
}