package entity;
import java.sql.*;
import com.google.gson.*;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
public class AuthorEntity{

	Long id;
	String firstName;
	String lastName;
	Collection<BookEntity> books = new ArrayList<>();

	public AuthorEntity(){

	}

	public AuthorEntity(Long id, String firstName, String lastName){
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public AuthorEntity(String firstName, String lastName){
		this.firstName = firstName;
		this.lastName = lastName;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId(){
		return id;
	}

	public String getFirstName(){
		return firstName;
	}

	public String getLastName(){
		return lastName;
	}

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "author")	
	public Collection<BookEntity> getBooks(){
		return books;
	}

	public void setId(Long id){
		this.id = id;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public void setLastName(String lastName ){
		this.lastName = lastName;
	}

	public void setBooks(Collection<BookEntity> books){
		this.books = books;
	}

	public void setBooks(BookEntity book){
		this.books.add(book);
	}

	public String toString(){
		return new Gson().toJson(this);
	}
}