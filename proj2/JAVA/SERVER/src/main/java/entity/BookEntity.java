package entity;
import java.sql.*;
import com.google.gson.*;
import javax.persistence.*;

@Entity
public class BookEntity{

	public Long id;
	public String isbn;
	public String title;
	public int publishedYear;
	public String publisher;
	public AuthorEntity author;

	public BookEntity(){

	}

	public BookEntity(Long id, String isbn, String title, int publishedYear, String publisher, AuthorEntity author){
		this.id = id; 
		this.isbn = isbn;
		this.title = title;
		this.publishedYear = publishedYear; 
		this.publisher = publisher;
		this.author = author;
	}

	public BookEntity(String isbn, String title, int publishedYear, String publisher, AuthorEntity author){
		this.isbn = isbn;
		this.title = title;
		this.publishedYear = publishedYear; 
		this.publisher = publisher;
		this.author = author;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId(){
		return id;
	}

	public String getIsbn(){
		return isbn;
	}
	
	public String getTitle(){
		return title;
	}

	public int getPublishedYear(){
		return publishedYear;
	}

	public String getPublisher(){
		return publisher;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	public AuthorEntity getAuthor(){
		return author;
	}

	public void setId(Long id){
		this.id = id;
	}

	public void setIsbn(String isbn){
		this.isbn = isbn;
	}
	
	public void setTitle(String title){
		this.title = title;
	}

	public void setPublishedYear(int publishedYear){
		this.publishedYear = publishedYear;
	}

	public void setPublisher(String publisher){
		this.publisher = publisher;
	}

	public void setAuthor(AuthorEntity author){
		this.author = author;
	}

	public String toString(){
		return new Gson().toJson(this);
	}
}