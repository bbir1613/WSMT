package repository;
import model.*;
import entity.*;
import java.util.*;
import java.sql.*;
import javax.persistence.*;
import java.util.stream.Collectors;

public class BookRepository{

	private String AUTHOR_TABLE = "AuthorEntity";
	private String BOOK_TABLE	= "BookEntity";
	private EntityManager entityManager;
	
	public BookRepository(EntityManager entityManager){
		this.entityManager = entityManager;
	}

	public BookEntity create(BookEntity bookEntity){
		EntityTransaction tr = entityManager.getTransaction();
		if(!tr.isActive()) tr.begin();
		entityManager.persist(bookEntity);
		entityManager.flush();			
		tr.commit();
		return bookEntity;
	}

	public Book update(Book book){
		BookEntity bookEntity = findBookById(book.id);
		EntityTransaction tr = entityManager.getTransaction();
		if(!tr.isActive()) tr.begin();
		bookEntity.setIsbn(book.isbn);
		bookEntity.setTitle(book.title);
		bookEntity.setPublishedYear(book.published_year);
		bookEntity.setPublisher(book.publisher);
		// AuthorEntity authorEntity = bookEntity.getAuthor();
		// authorEntity.setLasName(book.author.last_name);
		// authorEntity.setFirstName(book.author.first_name);
		// bookEntity.setAuthor(authorEntity);
		entityManager.flush();			
		tr.commit();
		return book;
	}

	public boolean delete(Long id){
		EntityTransaction tr = entityManager.getTransaction();
		if(!tr.isActive()) tr.begin();
	    String query = String.format("DELETE from %s  where id = '%d' ",BOOK_TABLE, id);
	    entityManager.createQuery(query).executeUpdate();
		entityManager.flush();			
		tr.commit();
		return true;
	}

	BookEntity findBookById(Long id){
		return findAllEntityBook().stream().filter(be-> be.getId() == id).findFirst().orElse(null);
	}
	
	List<BookEntity> findAllEntityBook(){
		EntityTransaction tr = entityManager.getTransaction();
		if(!tr.isActive()) tr.begin();
		String query = String.format("SELECT book from %s book ", BOOK_TABLE);
		List<BookEntity> booksEntity = entityManager.createQuery(query, BookEntity.class)
											.getResultList();
		entityManager.flush();			
		tr.commit();
		return booksEntity;
	}

	public List<Book> findAllBooks(){
		return findAllEntityBook()
			   .stream()
			   .map(be-> new Book(be.getId(), 
			   					  be.getIsbn(),
			   					  be.getTitle(),
			   					  be.getPublishedYear(), 
			   					  be.getPublisher(),
			   					  new Author(be.getAuthor().getId(),
			   					  			 be.getAuthor().getFirstName(),
			   					  			 be.getAuthor().getLastName())))
			   .collect(Collectors.toList());
		}

}	