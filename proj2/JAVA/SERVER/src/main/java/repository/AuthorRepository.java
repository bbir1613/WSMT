package repository;
import model.*;
import entity.*;
import java.util.*;
import java.sql.*;
import javax.persistence.*;
import java.util.stream.Collectors;

public class AuthorRepository{

	private String AUTHOR_TABLE = "AuthorEntity";
	private EntityManager entityManager;

	public AuthorRepository(EntityManager entityManager){
		this.entityManager = entityManager;
	}
	
	public Author create(Author author){
		AuthorEntity authorEntity= new AuthorEntity(author.first_name, author.last_name);
		EntityTransaction tr = entityManager.getTransaction();
		if(!tr.isActive()) tr.begin();
		entityManager.persist(authorEntity);
		entityManager.flush();			
		tr.commit();
		author.id = authorEntity.getId(); 
		return author;
	}

	public Author update(Author author){
		AuthorEntity authorEntity = finAuthorEntityById(author.id);
		EntityTransaction tr = entityManager.getTransaction();
		if(!tr.isActive()) tr.begin();
		authorEntity.setFirstName(author.first_name);
		authorEntity.setLastName(author.last_name);
		entityManager.flush();			
		tr.commit();
		return author;
	}

	public boolean delete(Long id){
		EntityTransaction tr = entityManager.getTransaction();
		if(!tr.isActive()) tr.begin();		
	    String query = String.format("DELETE from %s  where id = '%d' ",AUTHOR_TABLE, id);
	    entityManager.createQuery(query).executeUpdate();
		entityManager.flush();			
		tr.commit();
		return true;
	}

	public AuthorEntity findAuthorEntityByName(Author author){
		return findAllAuthorEntity()
			   .stream()
			   .filter(ae-> ae.getFirstName().equals(author.first_name) && ae.getLastName().equals(author.last_name))
		       .findFirst()
		       .orElse(null);
	}
	

	List<AuthorEntity> findAllAuthorEntity(){
		EntityTransaction tr = entityManager.getTransaction();
		if(!tr.isActive()) tr.begin();
		String query = String.format("SELECT author from %s author ",AUTHOR_TABLE);
		List<AuthorEntity> authors = entityManager.createQuery(query, AuthorEntity.class)
											.getResultList();
		entityManager.flush();			
		tr.commit();
		return authors;
	}
	
	AuthorEntity finAuthorEntityById(Long id){
		return findAllAuthorEntity()
			   .stream()
			   .filter(ae-> ae.getId() == id)
		       .findFirst()
		       .orElse(null);
	}

	public List<Author> findAllAuthors(){
		return findAllAuthorEntity().stream()
									.map(ae -> new Author(ae.getId(), ae.getFirstName(), ae.getLastName()))
									.collect(Collectors.toList());
	}

	public Author findAuthorByName(Author author){
		return findAllAuthorEntity()
			   .stream()
			   .filter(ae-> ae.getFirstName().equals(author.first_name) && ae.getLastName().equals(author.last_name))
			   .map(ae -> new Author(ae.getId(), ae.getFirstName(), ae.getLastName()))
		       .findFirst()
		       .orElse(null);
	}
}