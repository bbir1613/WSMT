import service.Service;
import net.Server;
import model.*;
import entity.*;
import repository.*;
import java.util.*;
import javax.jms.JMSException;
import javax.persistence.*;

public class JavaServer {
	
	public static void initDataBase(AuthorRepository authorRepository, BookRepository bookRepository){
		Author a1 = authorRepository.create(new Author(0L, "Mihai", "Eminescu"));
		Author a2 = authorRepository.create(new Author(0L, "Daniel", "Tulcea"));
		Author a3 =authorRepository.create(new Author(0L, "Magda", "Ianos"));

		AuthorEntity authorEntity = authorRepository.findAuthorEntityByName(a2);
		BookEntity bookEntity = new BookEntity("978-606-666-002-0", "Urme in vesnicie", 2013, "Doxologia", authorEntity);
		authorEntity.setBooks(bookEntity);
		bookRepository.create(bookEntity);

		authorEntity = authorRepository.findAuthorEntityByName(a2);
		bookEntity = new BookEntity("978-606-8117-98-0","Epifania",2011 ,"Doxologia", authorEntity);
		authorEntity.setBooks(bookEntity);
		bookRepository.create(bookEntity);

		authorEntity = authorRepository.findAuthorEntityByName(a2);
		bookEntity = new BookEntity("978-9732402115","Albatros",1991 ,"Albatros", authorEntity);
		authorEntity.setBooks(bookEntity);
		bookRepository.create(bookEntity);

		authorEntity = authorRepository.findAuthorEntityByName(a1);
		bookEntity = new BookEntity("978-973-128-453-8","Iubire, intlelepciune fara sfirsit",2014, "Corint Junior", authorEntity);
		authorEntity.setBooks(bookEntity);
		bookRepository.create(bookEntity);

		authorEntity = authorRepository.findAuthorEntityByName(a3);
		bookEntity = new BookEntity("973-9016-42-1", "Poezii", 1996, "Libra", authorEntity);
		authorEntity.setBooks(bookEntity);
		bookRepository.create(bookEntity);

		authorEntity = authorRepository.findAuthorEntityByName(a3);
		bookEntity = new BookEntity("-","Poezii",1988, "Minerva", authorEntity);
		authorEntity.setBooks(bookEntity);
		bookRepository.create(bookEntity);
	}
	
	public static void main(String[] args) throws JMSException {
		EntityManager entityManager= Persistence.createEntityManagerFactory("LibraryDatabase").createEntityManager();
		AuthorRepository authorRepository = new AuthorRepository(entityManager);
		BookRepository bookRepository = new BookRepository(entityManager);
		initDataBase(authorRepository, bookRepository);
		Service service = new Service(authorRepository, bookRepository);
		Server server = new Server(service.getRoutes());
		server.run();
		System.out.println("Type enter to exit");
		System.console().readLine();
		System.exit(0);
	}
}