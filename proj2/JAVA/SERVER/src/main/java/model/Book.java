package model;
import java.sql.*;
import com.google.gson.*;

public class Book{
	public Long id;
	public String isbn;
	public String title;
	public int published_year;
	public String publisher;
	public Author author;

	public Book(){

	}

	public Book(Long id, String isbn, String title, int published_year, String publisher, Author author){
		this.id = id; 
		this.isbn = isbn;
		this.title = title;
		this.published_year = published_year; 
		this.publisher = publisher;
		this.author = author;
	}

	public String toString(){
		return new Gson().toJson(this);
	}
}