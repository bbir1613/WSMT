package model;
import java.sql.*;
import com.google.gson.*;

public class Author{
	public Long id;
	public String first_name;
	public String last_name;

	public Author(){

	}

	public Author(Long id, String first_name, String last_name){
		this.id = id;
		this.first_name = first_name;
		this.last_name = last_name;
	}

	public String toString(){
		return new Gson().toJson(this);
	}
}