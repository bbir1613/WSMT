package service;
import java.util.Map;
import java.util.HashMap;
import util.HandleRequest;
import java.util.*;
import com.google.gson.*;
import model.*;
import entity.*;
import repository.*;

public class Service{

	public static String GET_BOOKS = "get/books";
	public static String GET_AUTHORS = "get/authors";
	public static String POST_BOOK = "post/book";
	public static String POST_AUTHOR = "post/author";
	public static String PUT_BOOK = "put/book";
	public static String PUT_AUTHOR = "put/author";
	public static String DELETE_BOOK = "delete/book";
	public static String DELETE_AUTHOR = "delete/authors";

	private Gson gson;
	private AuthorRepository authorRepository;
	private BookRepository bookRepository;

	public Service(AuthorRepository authorRepository, BookRepository bookRepository){
		this.authorRepository = authorRepository;
		this.bookRepository = bookRepository;
		gson = new Gson();
	}

	public String getBooks(){
		System.out.println("getBooks");
		List<Book> books = bookRepository.findAllBooks();

		return gson.toJson(books);
	}

	public String getAuthors(){
		System.out.println("getAuthors");
		List<Author> authors = authorRepository.findAllAuthors();

		return gson.toJson(authors);
	}

	public String postBook(Book book){
		System.out.println("postBook " + book.toString());
		AuthorEntity authorEntity = authorRepository.findAuthorEntityByName(book.author);
		if(authorEntity == null){
			authorRepository.create(book.author);	
			authorEntity = authorRepository.findAuthorEntityByName(book.author);
		} 
		BookEntity bookEntity = new BookEntity(book.isbn, book.title, book.published_year, book.publisher, authorEntity);
		authorEntity.setBooks(bookEntity);
		bookEntity = bookRepository.create(bookEntity);
		book.id = bookEntity.getId();
		return gson.toJson(book);
	}

	public String postAuthor(Author author){
		System.out.println("postAuthor " + author.toString());
		author = authorRepository.create(author);

		return gson.toJson(author);
	}

	public String putBook(Book book){
		System.out.println("putBook " + book.toString());
		authorRepository.update(book.author);
		bookRepository.update(book);

		return gson.toJson(book);
	}

	public String putAuthor(Author author){
		System.out.println("putAuthor " + author.toString());
		authorRepository.update(author);

		return gson.toJson(author);
	}

	public String deleteBook(Book book){
		System.out.println("deleteBook " + book.toString());
		bookRepository.delete(book.id);

		return "{ status : true }";
	}

	public String deleteAuthor(Author author){
		System.out.println("deleteAuthor " + author.toString());
		authorRepository.delete(author.id);

		return "{ status : true }";
	}


	public Map<String, HandleRequest> getRoutes(){
		Map<String, HandleRequest> routes = new HashMap<String, HandleRequest>(8);
		routes.put(GET_BOOKS, (String message)-> getBooks());
		routes.put(GET_AUTHORS, (String message)-> getAuthors());
		routes.put(POST_BOOK, (String message)-> postBook(gson.fromJson(message, Book.class)));
		routes.put(POST_AUTHOR, (String message)-> postAuthor(gson.fromJson(message, Author.class)));
		routes.put(PUT_BOOK, (String message)-> putBook(gson.fromJson(message, Book.class)));
		routes.put(PUT_AUTHOR, (String message)-> putAuthor(gson.fromJson(message, Author.class)));
		routes.put(DELETE_BOOK, (String message)-> deleteBook(gson.fromJson(message, Book.class)));
		routes.put(DELETE_AUTHOR, (String message)-> deleteAuthor(gson.fromJson(message, Author.class)));

		return routes;
	}

	public void run(){
		System.out.println("Service Hello world ");
	}

}