using CookComputing.XmlRpc;
using System;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

public struct Author
{
	public int id;
	public string firstName;
	public string lastName;

	public Author FromReader(MySqlDataReader reader)
	{
		id = reader.GetInt32(0);
		firstName = reader.GetString(1);
		lastName = reader.GetString(2);
		return this;
	}

	public override string ToString()
	{
		return String.Format("{{ {0}, {1}, {2} }}", id, firstName, lastName);  
	}
}

public struct Book
{
	public int id;
	public string isbn;
	public string title;
	public int publishedYear;
	public string published;
	public Author author;

	public Book FromReader(MySqlDataReader reader)
	{
		id = reader.GetInt32(0);
		isbn = reader.GetString(4);
		title = reader.GetString(5);
		published = reader.GetString(6);
		publishedYear = reader.GetInt32(7);
		author = new Author();
		author.id = reader.GetInt32(1); 
		author.firstName = reader.GetString(2);
		author.lastName = reader.GetString(3);
		return this;
	}


	public override string ToString()
	{
		return String.Format("{{ {0}, {1}, {2}, {3}, {4}, {5} }}", id, isbn, title, published, published, author.ToString());  
	}
}

public interface ILibrary
{
	[XmlRpcMethod("getBooks")]
	Book[] GetBooks();

	[XmlRpcMethod("getAuthors")]
	Author[] GetAuthors();

	[XmlRpcMethod("postBook")]
	Book PostBook(Book book);

	[XmlRpcMethod("postAuthor")]
	Author PostAuthor(Author author);

	[XmlRpcMethod("putBook")]
	Book PutBook(Book book);

	[XmlRpcMethod("putAuthor")]
	Author PutAuthor(Author author);

	[XmlRpcMethod("deleteBook")]
	void DeleteBook(int id);

	[XmlRpcMethod("deleteAuthor")]
	void DeleteAuthor(int id);

}