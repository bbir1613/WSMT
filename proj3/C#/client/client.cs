using System;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Collections.Generic;

using CookComputing.XmlRpc;

class _
{
  static void Main(string[] args)
  {
    HttpChannel chnl = new HttpChannel(null, new XmlRpcClientFormatterSinkProvider(), null);
    ChannelServices.RegisterChannel(chnl, false);
    ILibrary svr = (ILibrary)Activator.GetObject(typeof(ILibrary), "http://localhost:5678/statename.rem");

    {
      Console.WriteLine("GET BOOKS");
      Book[] books = svr.GetBooks();
      for(int i=0 ; i < books.Length; ++i)
      {
        Console.WriteLine(books[i]);
      }
    }
    
    {
      Console.WriteLine("GET AUTHORS");  
      Author[] authors = svr.GetAuthors();
      for(int i=0 ; i < authors.Length; ++i)
      {
        Console.WriteLine(authors[i]);
      }
    }
    
    {
      Console.WriteLine("POST BOOK");  
      Book postBook = new Book();
      postBook.id = 0;
      postBook.isbn = "isb";
      postBook.title = "title";
      postBook.publishedYear = 1924;
      postBook.published = "asd";
      Author author = new Author();
      author.id = 0;
      author.firstName = "FirstName";
      author.lastName = "LastName";
      postBook.author = author;

      Book book =  svr.PostBook(postBook);      
      Console.WriteLine(book);
      svr.DeleteBook(book.id);
      svr.DeleteAuthor(book.author.id);
    }

    {
      Console.WriteLine("POST AUTHOR");    
      Author postAuthor = new Author();
      postAuthor.id = 0;
      postAuthor.firstName = "FirstName";
      postAuthor.lastName = "LastName";

      Author author = svr.PostAuthor(postAuthor);
      Console.WriteLine(author);
      svr.DeleteAuthor(author.id);
    }

    {
      Console.WriteLine("PUT BOOK");          
      Book postBook = new Book();
      postBook.id = 0;
      postBook.isbn = "isb";
      postBook.title = "title";
      postBook.publishedYear = 1924;
      postBook.published = "asd";
      Author author = new Author();
      author.id = 0;
      author.firstName = "FirstName";
      author.lastName = "LastName";
      postBook.author = author;

      Book putBook =  svr.PostBook(postBook);      
      putBook.isbn = "isb";
      putBook.title = "title";
      putBook.publishedYear = 1924;
      putBook.published = "asd";

      Book book = svr.PutBook(putBook);      
      svr.DeleteBook(book.id);
      svr.DeleteAuthor(book.author.id);
      Console.WriteLine(book);
    }

    {
      Console.WriteLine("PUT AUTHOR");          
      Author postAuthor = new Author();
      postAuthor.id = 0;
      postAuthor.firstName = "FirstName";
      postAuthor.lastName = "LastName";
      Author putAuthor = svr.PostAuthor(postAuthor);
      putAuthor.firstName = "FirstName";
      putAuthor.lastName = "LastName";

      Author author = svr.PutAuthor(putAuthor);
      svr.DeleteAuthor(author.id);
      Console.WriteLine(author);
    }

  }
}