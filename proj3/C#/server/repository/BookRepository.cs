using System;
using System.Collections.Generic;
using MySql.Data;
using MySql.Data.MySqlClient;

public class BookRepository
{

  public static string BOOK_TABLE = "books";
  public static string AUTHOR_TABLE = "authors";
  public static string AUTHORS_TO_BOOK_TABLE = "authors_to_books";
  DatabaseConnection db = new DatabaseConnection();

  public Book Create(Book book)
  {
  	Console.WriteLine(String.Format("BookRepository Create: {0}", book));
    db.Open(connection=>
      {
        string query = String.Format(@"INSERT INTO {0}(isbn, title, published_year, publisher) 
                         VALUES('{1}', '{2}', '{3}', '{4}')",
                         BOOK_TABLE, book.isbn, book.title, book.publishedYear, book.published);
        Console.WriteLine(String.Format("BookRepository Create query: {0}", query));
        var cmd = new MySqlCommand(query, connection);
        cmd.ExecuteNonQuery();
        book.id = (int)cmd.LastInsertedId;
        query = String.Format(@"INSERT INTO {0}(author_id,book_id) valus('{1}', '{2}')", 
          AUTHORS_TO_BOOK_TABLE, book.author.id,book.id);
        cmd = new MySqlCommand(query, connection);
      });
  	Console.WriteLine(String.Format("BookRepository Create: {0}", book));
  	return book;
  }

  public bool Update(Book book)
  {
  	Console.WriteLine(String.Format("BookRepository Update: {0}", book));
    db.Open(connection=>
      {
        string query = String.Format(@"UPDATE {0} SET 
                         isbn = '{1}', 
                         title = '{2}',
                         published_year = '{3}', 
                         publisher = '{4}' 
                         where id = '{5}'",
                         BOOK_TABLE, book.isbn, book.title, book.publishedYear, book.published, book.id);
        Console.WriteLine(String.Format("BookRepository Update query: {0}", query));
        var cmd = new MySqlCommand(query, connection);
        cmd.ExecuteNonQuery();
      });

  	return true;
  }

  public bool Delete(int id)
  {
  	Console.WriteLine(String.Format("BookRepository Delete: {0}", id));
    db.Open(connection=>
      {
        string query = String.Format(@"DELETE from {0} where id = '{1}' ", BOOK_TABLE, id);
        Console.WriteLine(String.Format("BookRepository DELETE query: {0}", query));
        var cmd = new MySqlCommand(query, connection);
        cmd.ExecuteNonQuery();
      });

  	return true;
  }

  public List<Book> FindAllBooks()
  {
  	Console.WriteLine(String.Format("BookRepository FindAllBooks: "));
    List<Book> books = new List<Book>();
    db.Open(connection=>
      {
        string query = String.Format(@"SELECT book.id, author_id, first_name, last_name , isbn, title, publisher, published_year 
                         from {0} as book 
                         INNER JOIN {1} as ab 
                         INNER JOIN {2} as author
                         WHERE ab.book_id = book.id and author.id = ab.author_id ",
                          BOOK_TABLE, AUTHORS_TO_BOOK_TABLE, AUTHOR_TABLE);
        var cmd = new MySqlCommand(query, connection);
        var reader = cmd.ExecuteReader();
        if(reader.HasRows)
        {
          while(reader.Read())
          {
            books.Add(new Book().FromReader(reader));
          }
        }
      });

    return books;
  }

  public void Run()
  {
    Console.WriteLine("Hello BookRepository");
  }
  
}