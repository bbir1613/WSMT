using System;
using System.Threading;
using MySql.Data;
using MySql.Data.MySqlClient;

public class DatabaseConnection
{
	private static Mutex mutex = new Mutex();
	private	static MySqlConnection connection = new MySqlConnection("Server=localhost; database=csharpDatabase; UID=root; password=");
	public delegate void Callback(MySqlConnection connection);

	public void Open(Callback callback)
	{
		mutex.WaitOne();
		connection.Open();
		callback(connection);
		connection.Close();
		mutex.ReleaseMutex();
	}
	
	public void Run()
	{
		Console.WriteLine("Hello BookRepository");
	}  
}