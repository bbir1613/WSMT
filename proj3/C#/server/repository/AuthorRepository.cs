using System;
using System.Collections.Generic;
using System.Linq;
using MySql.Data;
using MySql.Data.MySqlClient;

public class AuthorRepository
{
	public static string AUTHOR_TABLE = "authors";
	DatabaseConnection db = new DatabaseConnection();

	public Author Create(Author author)
	{
		Console.WriteLine(String.Format("AuthorRepository Create: {0}", author));
		db.Open(connection=>
			{
				string query = String.Format(@"INSERT INTO {0}(first_name, last_name) 
	    							   VALUES('{1}','{2}')", AUTHOR_TABLE, author.firstName, author.lastName);
				Console.WriteLine(String.Format("AuthorRepository Create query: {0}", query));
			    var cmd = new MySqlCommand(query, connection);
			    cmd.ExecuteNonQuery();
			    author.id = (int)cmd.LastInsertedId;	
			});
		Console.WriteLine(String.Format("AuthorRepository Create: {0}", author));
		return author;
	}

	public bool Update(Author author)
	{
		Console.WriteLine(String.Format("AuthorRepository Update: {0}", author));
		db.Open(connection=>
			{
			    string query = String.Format(@"UPDATE {0} SET 
			    							   first_name = '{1}',
			    							   last_name = '{2}'
			    							   WHERE id = '{3}'", AUTHOR_TABLE, author.firstName, author.lastName, author.id);
				Console.WriteLine(String.Format("AuthorRepository Update query: {0}", query));
			    var cmd = new MySqlCommand(query, connection);
			    cmd.ExecuteNonQuery();
			});

		return true;
	}

	public bool Delete(int id)
	{
		Console.WriteLine(String.Format("AuthorRepository Delete: {0}", id));
		db.Open(connection=>
			{
			    string query = String.Format(@"DELETE from {0} WHERE id = '{1}'", AUTHOR_TABLE, id);
				Console.WriteLine(String.Format("AuthorRepository Delete query: {0}", query));
			    var cmd = new MySqlCommand(query, connection);
			    cmd.ExecuteNonQuery();
			});

		return true;
	}

	public List<Author> FindAllAuthors()
	{
		Console.WriteLine(String.Format("AuthorRepository FindAllAuthors: "));
		List<Author> authors = new List<Author>();	    
		db.Open(connection=>
			{
			    string query = String.Format(@"SELECT id, first_name, last_name from {0} ", AUTHOR_TABLE);
				Console.WriteLine(String.Format("AuthorRepository FindAllAuthors query: {0}", query));
			    var cmd = new MySqlCommand(query, connection);
			    var reader = cmd.ExecuteReader();
			    if(reader.HasRows)
			    {
			    	while(reader.Read())
			    	{
			    		authors.Add(new Author().FromReader(reader));	    		
			    	}
			    }
			});

		return authors;
	}

	public Author FindAuthorByName(Author author)
	{
		Console.WriteLine(String.Format("AuthorRepository FindAuthorByName: {0}", author));
		return FindAllAuthors().Find(a => a.firstName == author.firstName && a.lastName == author.lastName);
	}
	
	public List<Author> FindAllAuthorsByName(String firstName, String lastName)
	{
		Console.WriteLine(String.Format("AuthorRepository FindAuthorByName: {0} {1}", firstName, lastName));
		return FindAllAuthors().Where(a => a.firstName.Contains(firstName) || a.lastName.Contains(lastName)).ToList();		
	}
}