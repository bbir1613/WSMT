using System;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Collections.Generic;

using CookComputing.XmlRpc;


public class StateNameServer : MarshalByRefObject, ILibrary
{
  private AuthorRepository au = new AuthorRepository();
  private BookRepository bo = new BookRepository();

  public Book[] GetBooks()
  {
    Console.WriteLine("GetBooks");
    return bo.FindAllBooks().ToArray();
  }

  public Author[] GetAuthors()
  {
    Console.WriteLine("GetAuthors");
    return au.FindAllAuthors().ToArray();
  }

  public Book PostBook(Book book)
  {
    Console.WriteLine(String.Format("PostBook {0}", book));
    Author author = au.FindAuthorByName(book.author);
    if(author.id == 0)
    {
      author = au.Create(book.author);
    }
    book.author = author;
    return bo.Create(book);
  }

  public Author PostAuthor(Author author)
  {
    Console.WriteLine(String.Format("PostAuthor {0}", author));    
    return au.Create(author);
  }

  public Book PutBook(Book book)
  {
    Console.WriteLine(String.Format("PostBook {0}", book));
    au.Update(book.author);
    bo.Update(book);
    return book;
  }

  public Author PutAuthor(Author author)
  {
    Console.WriteLine(String.Format("PostAuthor {0}", author));    
    au.Update(author);
    return author;
  }

  public void DeleteBook(int id)
  {
    Console.WriteLine(String.Format("DeleteBook {0}", id));    
    bo.Delete(id);
  }

  public void DeleteAuthor(int id)
  {
    Console.WriteLine(String.Format("DeleteAuthor {0}", id));    
    au.Delete(id);
  }

}

class _
{
  static void Main(string[] args)
  {
    // var au= new AuthorRepository();
    // Author author =new Author();
    // author.id = 0;
    // author.firstName = "asd";
    // author.lastName = "test";
    // author = au.Create(author);
    // author.lastName = "huh";
    // au.Update(author);
    // Console.WriteLine(au.FindAuthorByName(author));
    // Console.WriteLine(au.FindAuthorByName(new Author()).id != 0);
    
    // var bo= new BookRepository(); 
    // bo.Run();
    // Book book = new Book();
    // book.author = author;
    // book.id = 1;
    // book.isbn = "isb";
    // book.title = "title";
    // book.publishedYear = 123;
    // book.published = "asd";
    // bo.Create(book);
    // book = bo.Create(book);
    // Console.WriteLine(book);
    // book.title = "eas";
    // bo.Update(book);
    // bo.Delete(book);
    // bo.FindAllBooks().ForEach(b => Console.WriteLine(b));
    // au.Delete(author.id);
    // au.FindAllAuthors().ForEach(a => Console.WriteLine(a));

    IDictionary props = new Hashtable();
    props["name"] = "MyHttpChannel";
    props["port"] = 5678;
    HttpChannel channel = new HttpChannel(
       props,
       null,
       new XmlRpcServerFormatterSinkProvider()
    );
    ChannelServices.RegisterChannel(channel, false);

    //RemotingConfiguration.Configure("StateNameServer.exe.config", false);
    RemotingConfiguration.RegisterWellKnownServiceType(
      typeof(StateNameServer),
      "statename.rem",
      WellKnownObjectMode.Singleton);
    Console.WriteLine("Press <ENTER> to shutdown");
    Console.ReadLine();
  }
}
