const Service = require('./service');
const Book = require('./model/Book');
const Author = require('./model/Author');

const main = async () =>{
	try{
		const service = new Service();
		console.log(await service.getBooks());
		console.log(await service.getAuthors());
		const postBook = JSON.parse(JSON.stringify(new Book({
	                 id: 0, 
	                 author: JSON.parse(JSON.stringify(new Author({id: 0, first_name:"FirstName", last_name:"LastName"}))), 
	                 isbn: 'isb', 
	                 title: 'title', 
	                 published_year: 1924, 
	                 publisher: 'asd'
	                })));
		const book = await service.postBook(postBook);
		console.log(book);
		await service.deleteBook(book.id);
		await service.deleteAuthor(book.author.id);
		const postAuthor = JSON.parse(JSON.stringify(new Author({id: 0, first_name:"FirstName", last_name:"LastName"})));
		const author = await service.postAuthor(postAuthor);
		await service.deleteAuthor(author.id);
		console.log(author);

	}catch(e){
		console.error(e);
	}
};

// main();