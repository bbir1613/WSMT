const XmlRpcClient = require('../net/XmlRpcClient');

class Service{

	constructor(){
		this.xmlClient = new XmlRpcClient();
	}

	async getBooks(){
		return await this.xmlClient.methodCall('getBooks');
	}

	async getAuthors(){
		return await this.xmlClient.methodCall('getAuthors');
	}

	async postBook(book){
		return await this.xmlClient.methodCall('postBook', book);
	}

	async deleteBook(bookId){
		return await this.xmlClient.methodCall('deleteBook',bookId);
	}

	async postAuthor(author){
		return await this.xmlClient.methodCall('postAuthor',author);
	}

	async deleteAuthor(authorId){
		return await this.xmlClient.methodCall('deleteAuthor',authorId);
	}
}

module.exports = Service;