class Author{
  constructor({id = undefined, first_name = undefined, last_name = undefined}){
    this.id = id
    this.firstName = first_name
    this.lastName = last_name
  }
}

module.exports = Author;