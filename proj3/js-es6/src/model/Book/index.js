class Book{
  constructor({id= undefined, author= undefined, isbn= undefined, title= undefined, 
               published_year= undefined, publisher= undefined}){
    this.id = id
    this.isbn = isbn
    this.title = title
    this.publishedYear = published_year
    this.published = publisher
    this.author = author    
  }
}

module.exports = Book;