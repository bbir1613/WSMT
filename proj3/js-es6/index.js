const xmlrpc = require('xmlrpc');

class XmlRpcClient{
	constructor(clientOptions = 'http://localhost:5678/statename.rem'){
		this.client = xmlrpc.createClient(clientOptions)		
	}

	methodCall(method,...body){
		return new Promise((res,rej)=>this.client.methodCall(method,body,(e,val)=>e? rej(e) : res(val)));
	}

}


class Book{
  constructor({id= undefined, author= undefined, isbn= undefined, title= undefined, 
               published_year= undefined, publisher= undefined}){
    this.id = id
    this.isbn = isbn
    this.title = title
    this.publishedYear = published_year
    this.published = publisher
    this.author = author    
  }
}

class Author{
  constructor({id = undefined, first_name = undefined, last_name = undefined}){
    this.id = id
    this.firstName = first_name
    this.lastName = last_name
  }
}

class Service{

	constructor(){
		this.xmlClient = new XmlRpcClient();
	}

	async getBooks(){
		return await this.xmlClient.methodCall('getBooks');
	}

	async getAuthors(){
		return await this.xmlClient.methodCall('getAuthors');
	}

	async postBook(book){
		return await this.xmlClient.methodCall('postBook', book);
	}

	async deleteBook(bookId){
		return await this.xmlClient.methodCall('deleteBook',bookId);
	}

	async postAuthor(author){
		return await this.xmlClient.methodCall('postAuthor',author);
	}

	async deleteAuthor(authorId){
		return await this.xmlClient.methodCall('deleteAuthor',authorId);
	}
}

const main = async () =>{
	try{
		const service = new Service();
		console.log(await service.getBooks());
		console.log(await service.getAuthors());
		const postBook = JSON.parse(JSON.stringify(new Book({
	                 id: 0, 
	                 author: JSON.parse(JSON.stringify(new Author({id: 0, first_name:"FirstName", last_name:"LastName"}))), 
	                 isbn: 'isb', 
	                 title: 'title', 
	                 published_year: 1924, 
	                 publisher: 'asd'
	                })));
		const book = await service.postBook(postBook);
		console.log(book);
		await service.deleteBook(book.id);
		await service.deleteAuthor(book.author.id);
		const postAuthor = JSON.parse(JSON.stringify(new Author({id: 0, first_name:"FirstName", last_name:"LastName"})));
		const author = await service.postAuthor(postAuthor);
		await service.deleteAuthor(author.id);
		console.log(author);

	}catch(e){
		console.error(e);
	}
};

// main();
function test(){
	console.log(` test`);
}