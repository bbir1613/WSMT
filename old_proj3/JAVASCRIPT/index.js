const xmlrpc = require('xmlrpc'); 
const clientOptions = 'http://localhost:5678/statename.rem';
const client = xmlrpc.createClient(clientOptions)

class Author{
  constructor({id = undefined, first_name = undefined, last_name = undefined}){
    this.id = id
    this.firstName = first_name
    this.lastName = last_name
  }
}

class Book{
  constructor({id= undefined, author= undefined, isbn= undefined, title= undefined, 
               published_year= undefined, publisher= undefined}){
    this.id = id
    this.isbn = isbn
    this.title = title
    this.publishedYear = published_year
    this.published = publisher
    this.author = author    
  }
}

// {
// 	client.methodCall('getBooks',null, (error,value)=>{  	
// 		if(error) {
// 			console.error(error)
// 			return;
// 		}
// 		console.log('getBooks ',value);
// 	});
// }

// {
// 	client.methodCall('getAuthors',null, (error,value)=>{
// 		if(error) {
// 			console.error(error)
// 			return;
// 		}
// 		console.log('getAuthors ',value);
// 	});
// }

// {
// 	const book = JSON.parse(JSON.stringify(new Book({
// 	                 id: 0, 
// 	                 author: JSON.parse(JSON.stringify(new Author({id: 0, first_name:"FirstName", last_name:"LastName"}))), 
// 	                 isbn: 'isb', 
// 	                 title: 'title', 
// 	                 published_year: 1924, 
// 	                 publisher: 'asd'
// 	                })));
// 	client.methodCall('postBook',[book], (error,value)=>{
// 		if(error) {
// 			console.error(error)
// 			return;
// 		}
// 		client.methodCall('deleteBook', [value.id], (error, value)=>{
// 			if(error) {
// 				console.error(error)
// 				return;
// 			}
// 		});
// 		client.methodCall('deleteAuthor', [value.author.id], (errr, value)=>{
// 			if(error) {
// 				console.error(error)
// 				return;
// 			}
// 		});
// 		console.log('postBook ',value);
// 	});
// }

// {
// 	const author = JSON.parse(JSON.stringify(new Author({id: 0, first_name:"FirstName", last_name:"LastName"})));
// 	client.methodCall('postAuthor',[author], (error,value)=>{
// 		if(error) {
// 			console.error(error)
// 			return;
// 		}
// 		client.methodCall('deleteAuthor', [value.id], (errr, value)=>{
// 			if(error) {
// 				console.error(error)
// 				return;
// 			}
// 		});
// 		console.log('postAuthor ',value);
// 	});
// }


// {
// 	const book = JSON.parse(JSON.stringify(new Book({
// 	                 id: 0, 
// 	                 author: JSON.parse(JSON.stringify(new Author({id: 0, first_name:"FirstName", last_name:"LastName"}))), 
// 	                 isbn: 'isb', 
// 	                 title: 'title', 
// 	                 published_year: 1924, 
// 	                 publisher: 'asd'
// 	                })));
// 	client.methodCall('postBook',[book], (error,value)=>{
// 		if(error) {
// 			console.error(error)
// 			return;
// 		}
// 		console.log('postBook ',value);
// 		client.methodCall('putBook',[value], (error,value)=>{
// 			if(error) {
// 				console.error(error)
// 				return;
// 			}
// 			client.methodCall('deleteBook', [value.id], (error, value)=>{
// 				if(error) {
// 					console.error(error)
// 					return;
// 				}
// 			});
// 			client.methodCall('deleteAuthor', [value.author.id], (errr, value)=>{
// 				if(error) {
// 					console.error(error)
// 					return;
// 				}
// 			});
// 			console.log('putBook',value);
// 		});
// 	});
// }

// {
// 	const author = JSON.parse(JSON.stringify(new Author({id: 0, first_name:"FirstName", last_name:"LastName"})));
// 	client.methodCall('postAuthor',[author], (error,value)=>{
// 		if(error) {
// 			console.error(error)
// 			return;
// 		}
// 		client.methodCall('putAuthor',[value], (error,value)=>{
// 				if(error) {
// 					console.error(error)
// 					return;
// 				}
// 				console.log('putAuthor ',value);
// 				client.methodCall('deleteAuthor', [value.id], (errr, value)=>{
// 					if(error) {
// 						console.error(error)
// 						return;
// 					}
// 				});
// 		});
// 		console.log('postAuthor ',value);
// 	});
// }
