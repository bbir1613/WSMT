const XmlRpcClient = require('../net/XmlRpcClient');

class Service{

	constructor(){
		this.xmlClient = new XmlRpcClient();
	}

	async getBooks(){
		return await this.xmlClient.methodCall('getBooks');
	}

	async getAuthors(){
		return await this.xmlClient.methodCall('getAuthors');
	}

	async getAuthorsByPartialName(name){
		return await this.xmlClient.methodCall('getAuthorsByPartialName', name);
	}

	async getBooksByPartialTitle(title){
		return await this.xmlClient.methodCall('getBooksByPartialTitle', title);
	}

	async addAuthorToBook(authorId, bookId){
		return await this.xmlClient.methodCall('addAuthorToBook', +authorId, +bookId);
	}

	async postBook(book){
		return await this.xmlClient.methodCall('postBook', book);
	}

	async putBook(book){
		return await this.xmlClient.methodCall('putBook', book);
	}

	async deleteBook(bookId){
		return await this.xmlClient.methodCall('deleteBook',+bookId);
	}

	async postAuthor(author){
		return await this.xmlClient.methodCall('postAuthor',author);
	}

	async putAuthor(author){
		return await this.xmlClient.methodCall('putAuthor',author);
	}

	async deleteAuthor(authorId){
		return await this.xmlClient.methodCall('deleteAuthor',+authorId);
	}
}

module.exports = Service;