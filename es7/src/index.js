const Service = require('./service');
const Ui = require('./ui');

const main = () =>{
  const service = new Service();
  const ui = new Ui(service);
};

main();