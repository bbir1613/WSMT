class Author{
  constructor(
  				id = undefined, 
  				firstName = undefined, 
  				lastName = undefined,
  				books = [],
  			){
    this.id = +id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.books = books;
  }

  toXml(){
    const obj ={id : this.id,
      firstName : this.firstName,
      lastName : this.lastName,
      books : JSON.parse(JSON.stringify(this.books)),
      };
      console.log(obj);
  	return JSON.parse(JSON.stringify(obj));
  }
}

module.exports = Author;