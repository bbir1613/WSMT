class Book{
  constructor(
  			  id= undefined,
   			  title= undefined, 
          isbn= undefined,
          publishedYear= undefined,
          publisher= undefined,
          authors = []){
    this.id = +id;
    this.isbn = isbn;
    this.title = title;
    this.publishedYear = +publishedYear;
    this.publisher = publisher;
    this.authors = authors;
  }

  toXml(){
  	const obj = {
		id: this.id, 
		isbn: this.isbn, 
		title: this.title, 
		publishedYear: this.publishedYear, 
		publisher: this.publisher,
    authors: JSON.parse(JSON.stringify(this.authors))
  	};
  	return JSON.parse(JSON.stringify(obj));
  }
}

module.exports = Book;