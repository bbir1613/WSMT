const xmlrpc = require('xmlrpc');

class XmlRpcClient{
	
	constructor(clientOptions = 'http://localhost:5678/statename.rem'){
		this.client = xmlrpc.createClient(clientOptions)		
	}

	methodCall(method,...body){
		return new Promise((res,rej)=>this.client.methodCall(method,body,(e,val)=>e?rej(e) : res(val)));
	}
}

module.exports = XmlRpcClient;