const Book = require('../model/Book');
const Author = require('../model/Author');

class Ui{
/*
postAuthor
putAuthor
*/
	constructor(service){
		this.service = service;
		document.getElementById('get_authors').addEventListener('click', this.getAuthors.bind(this));
		document.getElementById('get_books').addEventListener('click', this.getBooks.bind(this));
		document.getElementById('search_author_by_name')
		.addEventListener('submit',this.getAuthorsByPartialName.bind(this));
		document.getElementById('search_books_by_title')
		.addEventListener('submit',this.getBooksByPartialTitle.bind(this));
		document.getElementById('add_author_to_book')
		.addEventListener('submit',this.addAuthorToBook.bind(this));
		document.getElementById('delete_author_by_id')
		.addEventListener('submit',this.deleteAuthor.bind(this));
		document.getElementById('delete_book_by_id')
		.addEventListener('submit',this.deleteBook.bind(this));
		document.getElementById('add_author')
		.addEventListener('submit', this.addOrUpdateAuthor.bind(this));
		document.getElementById('add_book')
		.addEventListener('submit', this.addOrUpdateBook.bind(this));	
		this.authorList = document.getElementById('authors');
		this.bookList = document.getElementById('books');
	}

	async getBooks(e){
	    try{
		    console.log(`getBooks`);	
		    e && e.preventDefault();
		    console.log(`get books`);
		    const books = await this.service.getBooks();
		    console.log(books);
		    this.showBooks(books);
	    }catch(e){
	    	console.error(e);
	    }

	}

	async addOrUpdateBook(e){
		try{
			e && e.preventDefault();
			const bookId = e.target.bookId.value;
			const title = e.target.title.value;
			const ibsn = e.target.ibsn.value;
			const publishedYear = e.target.publishedYear.value;
			const publisher = e.target.publisher.value;
			const book = new Book(bookId,title,ibsn,publishedYear,publisher);
			if(book.id){			
				await this.service.putBook(book.toXml());
			}else{
				await this.service.postBook(book.toXml());
			}
			this.getAuthors();
			this.getBooks();
		}catch(e){
			console.error(e);
		}	
	}	
	
	async deleteBook(e){
		try{
			e && e.preventDefault();
			const bookId = e.target.bookId.value;
			console.log(` delete book `, bookId);
			await this.service.deleteBook(bookId);
			this.getAuthors();
			this.getBooks();
		}catch(e){
			console.error(e)
		}
	}
	
	async getBooksByPartialTitle(e){
		try{
			e && e.preventDefault();
			const title = e.target.title.value;
			console.log(` get Books By Partial Title `, title);
			const books = await this.service.getBooksByPartialTitle(title);
			console.log(books);
			this.showBooks(books);
		}catch(e){
			console.error(e);
		}
	}

	async getAuthors(e){
		try{
			console.log(`get authors`);
			e && e.preventDefault();
			const authors = await this.service.getAuthors();
			console.log(authors);
			this.showAuthors(authors);
		}catch(e){
			console.error(e);
		}		
	}

	async getAuthorsByPartialName(e){
		try{
			e && e.preventDefault();
			const name = e.target.name.value;
			console.log(`get Authors By Partial Name`, name);
			const authors = await this.service.getAuthorsByPartialName(name);
			console.log(authors);
			this.showAuthors(authors);
		}catch(e){
			console.error(e);
		}
	}

	async addOrUpdateAuthor(e){
		try{
			e && e.preventDefault();
			const authorId = e.target.authorId.value;
			const firstName = e.target.firstName.value;
			const lastName = e.target.lastName.value;
			const author = new Author(authorId, firstName, lastName);
			if(author.id){			
				await this.service.putAuthor(author.toXml());
			}else{
				console.log(` put author`);
				await this.service.postAuthor(author.toXml());
			}
			this.getAuthors();
			this.getBooks();
		}catch(e){
			console.error(e);
		}		
	}

	async addAuthorToBook(e){
		try{
			e && e.preventDefault();
			const authorId = e.target.authorId.value;
			const bookId = e.target.bookId.value;
			console.log(` add Author To Book `, authorId, bookId);
			await this.service.addAuthorToBook(authorId, bookId);
			this.getAuthors();
			this.getBooks();
		}catch(e){
			console.error(e);
		}
	}

	async deleteAuthor(e){
		try{
			e && e.preventDefault();
			const authorId = e.target.authorId.value;
			console.log(` delete author `, authorId);
			await this.service.deleteAuthor(authorId);
			this.getAuthors();
			this.getBooks();
		}catch(e){
			console.error(e)
		}
	}

	showAuthors(authors){
		const result = authors.map(au=>{
			const books = au.books.map(book=>{
				return(
					`<li>${book.id}</li>`+
					`<li>${book.title}</li>`+
					`<li>${book.isbn}</li>`+
					`<li>${book.publishedYear}</li>`+
					`<li>${book.publisher}</li>`);
			});
			return (
				`<li>${au.id}</li>`+
				`<li>${au.firstName}</li>`+
				`<li>${au.lastName}</li>`+
				`<li>books:</li>`+
				`<ul>${books}</ul>`);
		});
		this.authorList.innerHTML = `<ul>${result}</ul>`;
	}

	showBooks(books){
		const result = books.map(book=>{
			const authors = book.authors.map(author=>{
				return(
					`<li>${author.id}</li>`+
					`<li>${author.firstName}</li>`+
					`<li>${author.lastName}</li>`);
			});
			return(
				`<li>${book.id}</li>`+
				`<li>${book.title}</li>`+
				`<li>${book.isbn}</li>`+
				`<li>${book.publishedYear}</li>`+
				`<li>${book.publisher}</li>`+
				`<li>authors:</li>`+
				`<ul>${authors}</ul>`);
		});
		this.bookList.innerHTML = result;
	}
}

module.exports = Ui;
