create table authors	(
	id int not null primary key AUTO_INCREMENT,
	first_name varchar(255),
	last_name varchar(255)
);

create unique index idx_authors_id on authors(id);
create index idx_authors_first_name on authors(first_name);
create index idx_authors_last_name on authors(last_name);
	
create table books(
	id int not null primary key AUTO_INCREMENT,
	isbn varchar(255),
	title varchar(255),
	publisher varchar(255),
	published_year int
);

create unique index idx_id_books on books(id);
create index idx_title on books(title);
	
create table authors_to_books(
	author_id int not null,
	book_id int not null,
	primary key (author_id, book_id)
);

create unique index idx_authors_to_book on authors_to_books(author_id,book_id);

alter table authors_to_books add constraint fk_authors foreign key(author_id) references authors(id)
ON DELETE CASCADE ON UPDATE CASCADE;

alter table authors_to_books add constraint fk_books foreign key(book_id) references books(id)
ON DELETE CASCADE ON UPDATE CASCADE;

insert into authors(first_name, last_name) values ('Mihai', 'Eminescu');
insert into authors(first_name, last_name) values ('Daniel', 'Tulcea');
insert into authors(first_name, last_name) values ('Magda', 'Ianos');

insert into books(isbn, title, publisher, published_year) values ('978-606-666-002-0','Urme in vesnicie','Doxologia', 2013);
insert into books(isbn, title, publisher, published_year) values ('978-606-8117-98-0','Epifania','Doxologia', 2011);
insert into books(isbn, title, publisher, published_year) values ('978-9732402115','Albatros','Albatros', 1991);
insert into books(isbn, title, publisher, published_year) values ('978-973-128-453-8','Iubire, intlelepciune fara sfirsit','Corint Junior',2014);
insert into books(isbn, title, publisher, published_year) values ('973-9016-42-1','Poezii','Libra',	1996);
insert into books(isbn, title, publisher, published_year) values ('-','Poezii','Minerva',1988);
insert into books(isbn, title, publisher, published_year) values ('test','T1','T2',1);

insert into authors_to_books(author_id, book_id) values (2,1);
insert into authors_to_books(author_id, book_id) values (2,2);
insert into authors_to_books(author_id, book_id) values (2,3);
insert into authors_to_books(author_id, book_id) values (1,4);
insert into authors_to_books(author_id, book_id) values (3,5);
insert into authors_to_books(author_id, book_id) values (3,6);
