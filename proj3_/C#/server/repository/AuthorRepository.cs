using System;
using System.Collections.Generic;
using System.Linq;
using MySql.Data;
using MySql.Data.MySqlClient;

public class AuthorRepository
{
	DatabaseConnection db = new DatabaseConnection();

	public List<Author> FindAllAuthors()
	{
		Console.WriteLine(String.Format("AuthorRepository FindAllAuthors"));
		List<Author> authors = new List<Author>();	    
		db.Open(conn=>
			{
				string query = "select author.id, author.first_name, author.last_name from authors as author;";
				Console.WriteLine(String.Format("AuthorRepository FindAllAuthors query: {0}", query));
			    var cmd = new MySqlCommand(query, conn);
			    var reader = cmd.ExecuteReader();
			    if(reader.HasRows)
			    {
			    	while(reader.Read())
			    	{
			    		authors.Add(new Author().FromReader(reader));	    		
			    	}
			    }			
			});
		return authors;
	}

	public List<Author> FindAuthorsByPartialName(string name)
	{
		Console.WriteLine(String.Format("AuthorRepository FindAllAuthors"));
		List<Author> authors = new List<Author>();	    
		db.Open(conn=>
			{
				string query = String.Format(@"
					select author.id, author.first_name, author.last_name 
					from authors as author
					where author.first_name like '%{0}%' or author.last_name like '%{0}%';",name);
				Console.WriteLine(String.Format("AuthorRepository FindAllAuthors query: {0}", query));
			    var cmd = new MySqlCommand(query, conn);
			    var reader = cmd.ExecuteReader();
			    if(reader.HasRows)
			    {
			    	while(reader.Read())
			    	{
			    		authors.Add(new Author().FromReader(reader));	    		
			    	}
			    }			
			});
		return authors;
	}

	public List<Author> FindAuthorsByBookId(int id)
	{
		Console.WriteLine(String.Format("AuthorRepository FindAuthorsByBookId : {0}",id));
		List<Author> authors = new List<Author>();	    
		db.Open(conn=>
			{
				string query = String.Format(@"
					select author.id, author.first_name, author.last_name from authors as author 
					inner join authors_to_books as ab on ab.author_id = author.id 
					where ab.book_id = '{0}' ;",id);
				Console.WriteLine(String.Format("AuthorRepository FindAuthorsByBookId query: {0}", query));
			    var cmd = new MySqlCommand(query, conn);
			    var reader = cmd.ExecuteReader();
			    if(reader.HasRows)
			    {
			    	while(reader.Read())
			    	{
			    		authors.Add(new Author().FromReader(reader));	    		
			    	}
			    }			
			});
		return authors;
	}

	public Author Create(Author author)
	{
		Console.WriteLine(String.Format("AuthorRepository Create: {0}", author));
		db.Open(connection=>
			{
				string query = String.Format(@"insert into authors(first_name,last_name)
												values('{0}','{1}');", author.firstName, author.lastName);
				Console.WriteLine(String.Format("AuthorRepository Create query: {0}", query));
			    var cmd = new MySqlCommand(query, connection);
			    cmd.ExecuteNonQuery();
			    author.id = (int)cmd.LastInsertedId;	
			});
		Console.WriteLine(String.Format("AuthorRepository Create: {0}", author));
		return author;
	}

	public bool Update(Author author)
	{
		Console.WriteLine(String.Format("AuthorRepository Update: {0}", author));
		db.Open(connection=>
			{
			    string query = String.Format(@"UPDATE authors SET 
			    							   first_name = '{0}',
			    							   last_name = '{1}'
			    							   WHERE id = '{2}';", 
			    							   author.firstName, author.lastName, author.id);
				Console.WriteLine(String.Format("AuthorRepository Update query: {0}", query));
			    var cmd = new MySqlCommand(query, connection);
			    cmd.ExecuteNonQuery();
			});

		return true;
	}

	public void CreateAuthorToBook(int authorId, int bookId){
		Console.WriteLine(String.Format("CreateAuthorToBook Create: {0} {1}", authorId, bookId));
		db.Open(connection=>
			{
				string query = String.Format(@"insert into authors_to_books(author_id, book_id) 
											   values('{0}','{1}')",authorId, bookId);
				Console.WriteLine(String.Format("AuthorRepository Create query: {0}", query));
			    var cmd = new MySqlCommand(query, connection);
			    cmd.ExecuteNonQuery();
			});
	}

	public bool Delete(int id)
	{
		Console.WriteLine(String.Format("AuthorRepository Delete: {0}", id));
		db.Open(connection=>
			{
			    string query = String.Format(@"DELETE from authors WHERE id = '{0}'",id);
				Console.WriteLine(String.Format("AuthorRepository Delete query: {0}", query));
			    var cmd = new MySqlCommand(query, connection);
			    cmd.ExecuteNonQuery();
			});

		return true;
	}

}