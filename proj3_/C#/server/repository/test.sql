-- authors
-- books
-- authors_to_books

-- Biblioteca: Evidenta carti, autori:
-- * Introduceri / stergeri / modificari carti si autori
-- * Lista cartilor cu filtre dupa parti din titlu
-- * Lista autorilor cu filtre dupa parti din nume

-- get all authors;
select author.id, author.first_name, author.last_name 
from authors as author;

-- get all books for a specific author
select book.id, book.title, book.isbn, book.published_year, book.publisher 
from books as book 
inner join authors_to_books as ab on ab.book_id = book.id
where ab.author_id = '1';

-- get author by partial name
select author.id, author.first_name, author.last_name 
from authors as author
where author.first_name like '%Mih%' or author.last_name like "%Mih%";

select author.id, first_name, last_name, title, isbn, published_year, publisher 
from authors 
inner join authors_to_books as ab on authors.id = ab.author_id
inner join book on book.id = ab.book_id; 

insert into authors(first_name, last_name) values('test', 'test');
delete from authors where id = '';

-- get authors by book id
select author.id, author.first_name, author.last_name from authors as author 
inner join authors_to_books as ab on ab.author_id = author.id 
where ab.book_id = '2' 

-- get books by partial title
select book.id, book.title, book.isbn, book.published_year, book.publisher from books as book 
where book.title like '%a%' 