using System;
using System.Collections.Generic;
using MySql.Data;
using MySql.Data.MySqlClient;

public class BookRepository
{

  DatabaseConnection db = new DatabaseConnection();

  public List<Book> FindAllBooks()
  {
    Console.WriteLine(String.Format("BookRepository FindAllBooks"));
    List<Book> books = new List<Book>();
    db.Open(conn=>
      {
        string query = @"select book.id, book.title, book.isbn, book.published_year, book.publisher 
                         from books as book;";
        var cmd = new MySqlCommand(query, conn);
        var reader = cmd.ExecuteReader();
        if(reader.HasRows)
        {
          while(reader.Read())
          {
            books.Add(new Book().FromReader(reader));
          }
        }
      });  
      return books; 
  }

  public List<Book> FindBooksByAuthorId(int id)
  {
    Console.WriteLine(String.Format("BookRepository FindBookByAuthorId: {0}", id));
    List<Book> books = new List<Book>();
    db.Open(conn=>
      {
        string query = String.Format(
          @"select book.id, book.title, book.isbn, book.published_year, book.publisher 
            from books as book 
            inner join authors_to_books as ab on ab.book_id = book.id
            where ab.author_id = '{0}';", id);
        var cmd = new MySqlCommand(query, conn);
        var reader = cmd.ExecuteReader();
        if(reader.HasRows)
        {
          while(reader.Read())
          {
            books.Add(new Book().FromReader(reader));
          }
        }
      });  
      return books; 
  }
  
  public List<Book> FindBooksByPartialTitle(string title)
  {
    Console.WriteLine(String.Format("BookRepository FindBooksByPartialTitle: {0}", title));
    List<Book> books = new List<Book>();
    db.Open(conn=>
      {
        string query = String.Format(
          @"select book.id, book.title, book.isbn, book.published_year, book.publisher 
            from books as book 
            where book.title like '%{0}%';", title);
        var cmd = new MySqlCommand(query, conn);
        var reader = cmd.ExecuteReader();
        if(reader.HasRows)
        {
          while(reader.Read())
          {
            books.Add(new Book().FromReader(reader));
          }
        }
      });  
      return books; 
  }

  public Book Create(Book book)
  {
  	Console.WriteLine(String.Format("BookRepository Create: {0}", book));
    db.Open(connection=>
      {
        string query = String.Format(@"insert into books(isbn, title, published_year, publisher) 
                         VALUES('{0}', '{1}', '{2}', '{3}')",
                         book.isbn, book.title, book.publishedYear, book.publisher);
        Console.WriteLine(String.Format("BookRepository Create query: {0}", query));
        var cmd = new MySqlCommand(query, connection);
        cmd.ExecuteNonQuery();
        book.id = (int)cmd.LastInsertedId;
        cmd = new MySqlCommand(query, connection);
      });
  	Console.WriteLine(String.Format("BookRepository Create: {0}", book));
  	return book;
  }

  public bool Update(Book book)
  {
  	Console.WriteLine(String.Format("BookRepository Update: {0}", book));
    db.Open(connection=>
      {
        string query = String.Format(@"UPDATE books SET 
                         isbn = '{0}', 
                         title = '{1}',
                         published_year = '{2}', 
                         publisher = '{3}' 
                         where id = '{4}'",
                         book.isbn, book.title, book.publishedYear, book.publisher, book.id);
        Console.WriteLine(String.Format("BookRepository Update query: {0}", query));
        var cmd = new MySqlCommand(query, connection);
        cmd.ExecuteNonQuery();
      });
  	return true;
  }

  public bool Delete(int id)
  {
  	Console.WriteLine(String.Format("BookRepository Delete: {0}", id));
    db.Open(connection=>
      {
        string query = String.Format(@"DELETE from books where id = '{0}' ", id);
        Console.WriteLine(String.Format("BookRepository DELETE query: {0}", query));
        var cmd = new MySqlCommand(query, connection);
        cmd.ExecuteNonQuery();
      });

  	return true;
  }

}