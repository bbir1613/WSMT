using System;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Collections.Generic;

using CookComputing.XmlRpc;


public class StateNameServer : MarshalByRefObject, ILibrary
{
  private AuthorRepository au = new AuthorRepository();
  private BookRepository bo = new BookRepository();

  public Book[] GetBooks()
  {
    Console.WriteLine("GetBooks");
    List<Book> result = new List<Book>();
    bo.FindAllBooks().ForEach(bo=>
      {
        bo.authors = au.FindAuthorsByBookId(bo.id).ToArray();
        result.Add(bo);
      });
    return result.ToArray();
  }

public Book[] GetBooksByPartialTitle(string title)
  {
    Console.WriteLine(String.Format("GetBooksByPartialTitle : {0}", title));
    List<Book> result = new List<Book>();
    bo.FindBooksByPartialTitle(title).ForEach(bo=>
      {
        bo.authors = au.FindAuthorsByBookId(bo.id).ToArray();
        result.Add(bo);
      });
    return result.ToArray();
  }

  public Author[] GetAuthors()
  {
    Console.WriteLine("GetAuthors");
    List<Author> result =new List<Author>();
    au.FindAllAuthors().ForEach(au=>
      {
         au.books = bo.FindBooksByAuthorId(au.id).ToArray();
         result.Add(au);
      });
    return result.ToArray();
  }

  public Author[] GetAuthorsByPartialName(string name)
  {
    Console.WriteLine(String.Format("GetAuthorsByPartialName : {0} ", name));
    List<Author> result =new List<Author>();
    au.FindAuthorsByPartialName(name).ForEach(au=>
      {
         au.books = bo.FindBooksByAuthorId(au.id).ToArray();
         result.Add(au);
      });
    return result.ToArray();
  }

  public void AddAuthorToBook(int authorId,int bookId){
    Console.WriteLine(String.Format("AddAuthorToBook: {0}, {1}", authorId, bookId));
    au.CreateAuthorToBook(authorId, bookId);
  }

  public Book PostBook(Book book)
  {
    Console.WriteLine(String.Format("PostBook {0}", book));
    return bo.Create(book);
  }

  public Author PostAuthor(Author author)
  {
    Console.WriteLine(String.Format("PostAuthor {0}", author));    
    return au.Create(author);
  }

  public Book PutBook(Book book)
  {
    Console.WriteLine(String.Format("PutBook {0}", book));
    bo.Update(book);
    return book;
  }

  public Author PutAuthor(Author author)
  {
    Console.WriteLine(String.Format("PostAuthor {0}", author));    
    au.Update(author);
    return author;
  }

  public void DeleteBook(int id)
  {
    Console.WriteLine(String.Format("DeleteBook {0}", id));    
    bo.Delete(id);
  }

  public void DeleteAuthor(int id)
  {
    Console.WriteLine(String.Format("DeleteAuthor {0}", id));    
    au.Delete(id);
  }

}

class _
{
  static void Main(string[] args)
  {
    // var x = new StateNameServer();
    // x.GetAuthors();

    IDictionary props = new Hashtable();
    props["name"] = "MyHttpChannel";
    props["port"] = 5678;
    HttpChannel channel = new HttpChannel(
       props,
       null,
       new XmlRpcServerFormatterSinkProvider()
    );
    ChannelServices.RegisterChannel(channel, false);

    //RemotingConfiguration.Configure("StateNameServer.exe.config", false);
    RemotingConfiguration.RegisterWellKnownServiceType(
      typeof(StateNameServer),
      "statename.rem",
      WellKnownObjectMode.Singleton);
    Console.WriteLine("Press <ENTER> to shutdown");
    Console.ReadLine();
  }
}
