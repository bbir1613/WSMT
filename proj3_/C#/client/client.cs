using System;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Collections.Generic;

using CookComputing.XmlRpc;

class _
{
  static void Main(string[] args)
  {
    HttpChannel chnl = new HttpChannel(null, new XmlRpcClientFormatterSinkProvider(), null);
    ChannelServices.RegisterChannel(chnl, false);
    ILibrary svr = (ILibrary)Activator.GetObject(typeof(ILibrary), "http://localhost:5678/statename.rem");
    
    Book book = new Book();
    book.title = "Test title";
    book.isbn = "Test isbn";
    book.publishedYear = 1955;
    book.publisher = "Test publisher";
    book.authors = new Author[0];

    Author author = new Author();
    author.firstName = "Test firstName";
    author.lastName = "Test lastName";
    author.books = new Book[0];
    
    book = svr.PostBook(book);
    author = svr.PostAuthor(author);

    svr.DeleteBook(book.id);
    svr.DeleteAuthor(author.id);
  }
}