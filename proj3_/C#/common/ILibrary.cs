using CookComputing.XmlRpc;
using System;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

public struct Author
{
	public int id;
	public string firstName;
	public string lastName;
	public Book[] books;

	public Author FromReader(MySqlDataReader reader)
	{
		id = reader.GetInt32(0);
		firstName = reader.GetString(1);
		lastName = reader.GetString(2);
		books = new Book[0];
		return this;
	}
}

public struct Book
{
	public int id;
	public string title;
	public string isbn;
	public int publishedYear;
	public string publisher;
	public Author[] authors;

	public Book FromReader(MySqlDataReader reader)
	{
		id = reader.GetInt32(0);
		title = reader.GetString(1);
		isbn = reader.GetString(2);
		publishedYear = reader.GetInt32(3);
		publisher = reader.GetString(4);
		authors = new Author[0];
		return this;
	}
}

public interface ILibrary
{
	[XmlRpcMethod("getBooks")]
	Book[] GetBooks();

	[XmlRpcMethod("getBooksByPartialTitle")]
	Book[] GetBooksByPartialTitle(string title);

	[XmlRpcMethod("getAuthors")]
	Author[] GetAuthors();

	[XmlRpcMethod("getAuthorsByPartialName")]
	Author[] GetAuthorsByPartialName(string name);

	[XmlRpcMethod("addAuthorToBook")]
	void AddAuthorToBook(int authorId, int bookId);

	[XmlRpcMethod("postBook")]
	Book PostBook(Book book);

	[XmlRpcMethod("postAuthor")]
	Author PostAuthor(Author author);

	[XmlRpcMethod("putBook")]
	Book PutBook(Book book);

	[XmlRpcMethod("putAuthor")]
	Author PutAuthor(Author author);

	[XmlRpcMethod("deleteBook")]
	void DeleteBook(int id);

	[XmlRpcMethod("deleteAuthor")]
	void DeleteAuthor(int id);

}